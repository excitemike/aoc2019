import Control.Concurrent
import Control.Monad
import Data.Char
import System.IO

main :: IO()
main = do
    input <- readFile "day16input"
    let baseSignal = map digitToInt input
    let p2Offset = (read::String->Int) $ take 7 input
    let signalLength = 10000 * (length baseSignal)
    let truncatedLength = signalLength - p2Offset
    let dropCount = mod p2Offset (length baseSignal)
    putStrLn
      $ ("part 1: " ++)
      $ concat
      $ map show
      $ take 8
      $ p1Phases 100
      $ baseSignal
    putStrLn
      $ ("part 2: " ++)
      $ concat
      $ map show
      $ take 8
      $ reverse
      $ p2Phases 100
      $ reverse
      $ take truncatedLength
      $ drop dropCount
      $ concat
      $ replicate 10000
      $ baseSignal
  where basePattern = [0,1,0,-1]
        p1Pattern :: Int->[Int]
        p1Pattern n = drop 1 $ concat $ repeat $ concat $ map (replicate (n+1)) $ basePattern
        p1FftPhaseForPlace :: [Int]->Int->Int
        p1FftPhaseForPlace input n = (\x->mod (abs x) 10)
                                 $ sum
                                 $ zipWith (*) 
                                           (take (length input) $ p1Pattern n) 
                                           input
        p1FftPhase :: [Int]->[Int]
        p1FftPhase input = map (p1FftPhaseForPlace input) [0..((length input)-1)]
        p1Phases :: Int->[Int]->[Int]
        p1Phases n input | n <= 0    = input
                         | otherwise = (p1Phases (n-1) $ p1FftPhase input)
        p2PhaseInt :: Int->[Int]->[Int]
        p2PhaseInt s_nplus1 [] = []
        p2PhaseInt s_nplus1 (x:xs) = 
            let s_n = (mod (s_nplus1 + x) 10)
            in (s_n : (p2PhaseInt s_n xs))
        p2Phase :: [Int]->[Int]
        p2Phase [] = []
        p2Phase (x:xs) = (x : (p2PhaseInt x xs))
        p2Phases :: Int->[Int]->[Int]
        p2Phases n input | n <= 0    = input
                         | otherwise = p2Phases (n-1) $ p2Phase input
      