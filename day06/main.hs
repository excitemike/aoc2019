import Data.Char
import Data.Function
import System.IO

import qualified Data.Map as Map


type WhoOrbitsWho = Map.Map String String


-- list of what <who} orbits, directly or not, in order from COM to the one it directly orbits
orbitChain :: WhoOrbitsWho->String->[String]
orbitChain orbitTable who =
  case Map.lookup who orbitTable of
    Just nextWho -> (orbitChain orbitTable nextWho) ++ [nextWho]
    Nothing -> []


stepsToRoot :: WhoOrbitsWho->String->Int
stepsToRoot whoOrbitsWho who =
  case Map.lookup who whoOrbitsWho of 
    Just nextWho -> 1 + stepsToRoot whoOrbitsWho nextWho
    Nothing -> 0


day06 :: String->(Int, Int)
day06 input = 
  let
    { orbitTable = Map.fromList
      $ map ((\(a, _:b) -> (b, a)) . (break (==')')))
      $ lines input
    ; p1 = sum $ map (stepsToRoot orbitTable) $ Map.keys orbitTable
    ; myChain = orbitChain orbitTable "YOU"
    ; santaChain = orbitChain orbitTable "SAN"
    ; sharedLength = length
      $ takeWhile (\(a,b) -> a==b)
      $ zip myChain santaChain
    ; p2 = (length myChain) + (length santaChain) - 2 * sharedLength
    }
  in (p1,p2)


main :: IO()
main = do
  input <- readFile "day06input"
  --let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L"
  let (p1, p2) = day06 input
  putStrLn ("part 1: " ++ (show p1))
  putStrLn ("part 2: " ++ (show p2))

  