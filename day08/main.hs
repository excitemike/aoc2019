import Data.Char
import Data.Function
import System.IO


type Sif = [Layer]
type Layer = [Row]
type Row = String


convertChar :: Char->Char
convertChar c
  | c == '0' = ' '
  | c == '1' = '█'
  | c == '2' = '▒'
  | otherwise = '?'


readRow :: Int->String->([Char], String)
readRow numColumns s
    | null s          = ([], "")
    | numColumns <= 0 = ([], s)
    | numColumns == 1 = (convertChar c:[], s2)
    | otherwise       = (convertChar c:restOfRow, s3)
  where
    c:s2           = s
    (restOfRow,s3) = readRow (numColumns-1) s2


readLayer :: (Int,Int)->String->([[Char]], String)
readLayer (numRows,numColumns) s
    | null s       = ([], "")
    | numRows <= 0 = ([[]], "")
    | numRows == 1 = (row:[], s2)
    | otherwise    = (row:restOfLayer, s3)
  where
    (row, s2) = readRow numColumns s
    (restOfLayer, s3) = readLayer (numRows-1, numColumns) s2


readSpaceImageFormat :: (Int,Int)->String->Sif
readSpaceImageFormat shape s 
    | null s = []
    | otherwise = layer : (readSpaceImageFormat shape s2)
  where (layer, s2) = readLayer shape s


countInRow :: Char->Row->Int
countInRow c row = length $ (filter (==c)) row


countInLayer :: Char->Layer->Int
countInLayer c layer = sum $ map (countInRow c) layer


argmin :: Ord b => (a->b)->[a]->a
argmin f xs
    | null xs   = error ":("
    | otherwise = foldl (\x1 x2-> if (f x1)<(f x2) then x1 else x2) x xs'
  where
    x:xs' = xs


combineRows :: Row->Row->Row
combineRows = zipWith (\c1 c2 -> if c1=='▒' then c2 else c1) 


combineLayers :: Layer->Layer->Layer
combineLayers = zipWith combineRows


main :: IO()
main = do
    input <- readFile "day08input"
    let sif = readSpaceImageFormat (6,25) input
    putStrLn
      $ ("part 1: " ++)
      $ show
      $ (\x -> (countInLayer '█' x) * (countInLayer '▒' x))
      $ argmin (countInLayer ' ') sif
    putStrLn "part 2: "
    putStrLn
      $ concat
      $ map (foldr (\a b -> a:a:b) "\n")  -- double the characters in each line and adds a newline
      $ foldl combineLayers (sif!!0) (drop 1 sif)