import System.IO

import qualified VM9

readTape :: String->[Int]
readTape s =
  let s2 = dropWhile (==',') s
  in
    if null s2
    then []
    else case span (/=',') s2 of
      ("","") -> []
      (a,"") -> (read a):[]
      ("",b) -> readTape b
      (a,_:b) -> (read a):(readTape b)


main :: IO()
main = do
    let t1input = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
    putStrLn
      $ ("test 1: " ++)
      $ show
      $ (==t1input)
      $ VM9.outputs
      $ VM9.runVM
      $ VM9.makeVM t1input [1]
    let t2input = [1102,34915192,34915192,7,4,7,99,0]
    putStrLn
      $ ("test 2: " ++)
      $ show
      $ (==1219070632396864) . (!!0)
      $ VM9.outputs
      $ VM9.runVM
      $ VM9.makeVM t2input [1]
    let t3input = [104,1125899906842624,99]
    putStrLn
      $ ("test 3: " ++)
      $ show
      $ (==1125899906842624) . (!!0)
      $ VM9.outputs
      $ VM9.runVM
      $ VM9.makeVM t3input [1]
    input <- readFile "day09input"
    putStrLn
      $ ("part 1: " ++)
      $ show
      $ VM9.outputs
      $ VM9.runVM
      $ VM9.makeVM (readTape input) [1]
    putStrLn "part 2: "
    putStrLn
      $ show
      $ VM9.outputs
      $ VM9.runVM
      $ VM9.makeVM (readTape input) [2]