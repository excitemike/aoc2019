import Data.Char
import System.IO 
import Control.Monad

import qualified Data.Map.Strict as Map

import Data.Set (Set)
import qualified Data.Set as Set

eat_comma :: String->String
eat_comma s = 
  if (null s)
  then s
  else let c = s !! 0
    in if (c==',')
    then (drop 1 s)
    else s

read_comma_separated_ints :: String->[Int]
read_comma_separated_ints s = 
  if (null s)
  then []
  else let (i, s2) = (read_int s)
    in (i : (read_comma_separated_ints $ eat_comma s2))

read_digits :: String->(String,String)
read_digits s = 
  if (null s)
  then ("","")
  else let c = s !! 0
    in
      if (isDigit c)
      then let (c2, s2) = (read_digits (drop 1 s))
        in (c : c2, s2)
      else ("", s)

read_int :: String->(Int,String)
read_int s = let (s1, s2) = (read_digits s)
  in
    ((read::String->Int) s1, s2)

type Step = (Char,Int)
type Point = (Int,Int)

read_path_step :: String->(Step, String)
read_path_step s = 
  let {c = s !! 0;
       s2 = drop 1 s;
       (i, s3) = read_int s2}
  in ((c, i), s3)

read_wire_path :: String->[Step]
read_wire_path s = 
  if (null s)
  then []
  else let (step, s2) = (read_path_step s)
    in (step : (read_wire_path (eat_comma s2)))

step_point :: Int->Int->Char->Int->(Int, Int, Char, Int)
step_point x y direction num_steps =
  if (num_steps <= 0)
  then (x, y, direction, 0) 
  else
    case direction of
      'U' -> (x, y+1, direction, num_steps-1)
      'D' -> (x, y-1, direction, num_steps-1)
      'L' -> (x-1, y, direction, num_steps-1)
      'R' -> (x+1, y, direction, num_steps-1)

insert_points_for_step :: Int -> Int -> Char -> Int -> Int -> (Map.Map Point Int) -> (Int, Int, Int, Map.Map Point Int)
insert_points_for_step x y direction num_steps steps_taken map_so_far =
  if num_steps <= 0
  then (x, y, steps_taken, map_so_far)
  else 
    let { (x2, y2, _, num_steps2) = (step_point x y direction num_steps);
          p = (x2, y2);
          steps_taken2 = (steps_taken+1);
          updated_map = (Map.insert p steps_taken2 map_so_far) }
    in
      insert_points_for_step x2 y2 direction num_steps2 steps_taken2 updated_map

steps_to_point_map :: Int->Int->[Step]->Int->Map.Map Point Int->Map.Map Point Int
steps_to_point_map x y steps steps_taken map_so_far = 
  if (null steps)
  then map_so_far
  else 
    let { (direction, num_steps) = (steps !! 0);
          (x2, y2, steps_taken2, map2) = (insert_points_for_step x y direction num_steps steps_taken map_so_far) }
    in 
      steps_to_point_map x2 y2 (drop 1 steps) steps_taken2 map2

manhattan_distance :: Point->Int
manhattan_distance (x, y) = abs(x) + abs(y)

extract_keys :: Map.Map k v->[k]
extract_keys = Map.foldlWithKey (\keys key val -> key : keys) []

combined_distance :: [Map.Map Point Int]->Point->Int
-- look up key in all the maps
combined_distance maps key = 
  let lookup = Map.findWithDefault (-1) key
  in sum $ map lookup maps

main :: IO()
main = do
  input <- readFile "day03input"
  --let input = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"
  --let input = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
  let wire_paths = map read_wire_path (lines input)
  let wire_points = map (\step_list -> steps_to_point_map 0 0 step_list 0 Map.empty) wire_paths
  let intersections = extract_keys $ Map.intersection (wire_points !! 0) (wire_points !! 1)
  let p1_distances = map manhattan_distance intersections
  let p1 = minimum p1_distances
  putStrLn ("part 1: " ++ (show p1))
  let p2_distances = map (combined_distance wire_points) intersections
  let p2 = minimum p2_distances
  putStrLn ("part 2: " ++ (show p2))