import Data.Array
import Data.List
import qualified Data.Map.Strict as Map
import System.IO

type Asteroid = (Int, Int)
type AsteroidPair = (Asteroid, Asteroid)
type Slope = (Int, Int)

argmax :: Ord b => (a->b)->[a]->a
argmax f xs
    | null xs   = error ":("
    | otherwise = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'
  where
    x:xs' = xs


argmin :: Ord b => (a->b)->[a]->a
argmin f xs
    | null xs   = error ":("
    | otherwise = foldl (\x1 x2-> if (f x1)<(f x2) then x1 else x2) x xs'
  where
    x:xs' = xs

readAsteroidsLine :: Int->String->[Asteroid]
readAsteroidsLine row s =
    map ((\col->(col,row)) . fst)
    $ filter ((=='#') . snd)
    $ zipWith (\col char->(col,char)) [0,1..] s


readAsteroids :: String->[Asteroid]
readAsteroids s =
  foldl (++) []
  $ zipWith readAsteroidsLine [0,1..] (lines s)


simplifyFrac :: Slope->Slope
simplifyFrac (numerator, denominator) =
    if denominator==0
    then (if numerator<0 then -1 else 1,0)
    else (quot numerator divisor,
          quot denominator divisor)
  where divisor = gcd numerator denominator


calcSlope :: Asteroid->Asteroid->Slope
calcSlope (x1,y1) (x2,y2) = simplifyFrac (y2-y1, x2-x1)


lineOfSightFilter :: Asteroid->[Asteroid]->[Asteroid]
lineOfSightFilter start [] = []
lineOfSightFilter start (first:rest) =
    first:(filter f $ lineOfSightFilter start rest)
  where
    cullSlope = calcSlope start first
    f = (/=cullSlope). (calcSlope start)


asteroidsInLineOfSight :: Asteroid->[Asteroid]->[Asteroid]
asteroidsInLineOfSight from allAsteroids =
    lineOfSightFilter from [ a | a <- allAsteroids, a/=from ]


heading :: Asteroid->Asteroid->Float
heading (x1,y1) (x2,y2)
    | x1 > x2 = theta + 2 * pi
    | otherwise = theta
  where
    theta :: Float
    theta = atan2 (fromIntegral (x2-x1)) (fromIntegral (y1-y2))


main :: IO()
main = do
    putStrLn "test 1:"
    putStrLn
      $ unlines
      $ map show
      $ Map.toList
      $ Map.map length
      $ Map.mapWithKey lineOfSightFilter
      $ Map.fromList [ (a, filter (/=a) test1Asteroids) | a <- test1Asteroids ]
    input <- readFile "day10input"
    let asteroids = readAsteroids input
    -- it's slow. answer hardcoded below
    let p1 = if False
             then partOne asteroids
             else ((19, 14), 274)
    let shootFrom = fst p1
    putStrLn $ "part 1: " ++ (show p1)
    putStrLn $ ("part 2: " ++)
        $ show
        $ (\(x,y)->(x*100+y))
        $ (argmin (distanceSq shootFrom))
        $ (\slope -> filter ((==slope) . (calcSlope shootFrom)) asteroids)
        $ (calcSlope shootFrom)
        $ (!!199)
        $ sortOn (heading shootFrom)
        $ asteroidsInLineOfSight shootFrom asteroids
  where
    test1Asteroids = readAsteroids ".#..#\n.....\n#####\n....#\n...##"
    partOne = \asteroids 
      -> argmax snd
      $ Map.toList
      $ Map.map length
      $ Map.mapWithKey lineOfSightFilter
      $ Map.fromList [ (a, filter (/=a) asteroids) | a <- asteroids ]
    distanceSq :: Asteroid->Asteroid->Int
    distanceSq (x1,y1) (x2,y2) = ((x2-x1)*(x2-x1)) + ((y2-y1)*(x2-x1))
