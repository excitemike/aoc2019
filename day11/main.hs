import Control.Concurrent
import Control.Monad
import Data.Array
import Data.List
import qualified Data.IntMap.Strict as Map
import System.IO
import System.Process

import qualified VM9

type XY = (Int,Int)

type PaintedPanels = Map.IntMap (Map.IntMap Int)

data Direction = DirUp | DirDown | DirLeft | DirRight
  deriving(Show)

data State = State
  { vm :: VM9.VM
  , direction :: Direction
  , xy :: XY
  , paintedPanels :: PaintedPanels
  }
  deriving(Show)


blackPaintCode = 0
blackPaintStr = "▒▒"
whitePaintCode = 1
whitePaintStr = "██"


readTape :: String->[Int]
readTape s =
  let s2 = dropWhile (==',') s
  in
    if null s2
    then []
    else case span (/=',') s2 of
      ("","") -> []
      (a,"") -> (read a):[]
      ("",b) -> readTape b
      (a,_:b) -> (read a):(readTape b)


turnLeft :: Direction->Direction
turnLeft DirUp    = DirLeft
turnLeft DirLeft  = DirDown
turnLeft DirDown  = DirRight
turnLeft DirRight = DirUp


turnRight :: Direction->Direction
turnRight DirUp    = DirRight
turnRight DirRight = DirDown
turnRight DirDown  = DirLeft
turnRight DirLeft  = DirUp


turn :: Int->Direction->Direction
turn 0 = turnLeft
turn 1 = turnRight


move :: Direction->XY->XY
move DirUp    (x,y) = (x, y-1)
move DirLeft  (x,y) = (x-1, y)
move DirDown  (x,y) = (x, y+1)
move DirRight (x,y) = (x+1, y)


doPaint :: XY->Int->PaintedPanels->PaintedPanels
doPaint (x,y) paintCode paintedPanels =
    let { row = Map.findWithDefault Map.empty y paintedPanels
        ; row2 = Map.insert x paintCode row
        }
    in Map.insert y row2 paintedPanels


checkPaint :: XY->PaintedPanels->Int
checkPaint (x,y) paintedPanels =
    case Map.lookup y paintedPanels of
      Just row -> case Map.lookup x row of
                    Just x -> x
                    Nothing -> blackPaintCode
      Nothing -> blackPaintCode


paintCodeToStr :: Int->String
paintCodeToStr 0 = blackPaintStr
paintCodeToStr 1 = whitePaintStr
paintCodeToStr c = error $ "found paintCode" ++ (show c)


clear :: IO()
clear = (system "cls") >> return ()

draw :: State->IO()
draw state = do
    putStrLn 
      $ concat
      $ map rowStr
      $ range (minY,maxY)
    putStrLn ""
  where
    panels = paintedPanels state
    minX = minimum $ map (fst . Map.findMin . snd) $ Map.toList panels
    maxX = maximum $ map (fst . Map.findMax . snd) $ Map.toList panels
    minY = fst $ Map.findMin panels
    maxY = fst $ Map.findMax panels
    rowStr y = (++"\n")
             $ concat
             $ map ( paintCodeToStr
                   . (\x -> checkPaint (x,y) panels)
                   )
             $ range (minX,maxX)


stepRobot :: State->IO State
stepRobot state =
    return state2
  where 
    paintCode = checkPaint (xy state) (paintedPanels state)
    vm2 = (vm state) { VM9.inputs = [paintCode] }
    (vm3, outputs) = VM9.runUntilBlocked vm2
    paintedPanels2 =
        if (length outputs) < 2
        then error $ "missing output " ++ (show $ outputs)
        else doPaint (xy state) (outputs!!0) (paintedPanels state)
    direction2 =
        if (length outputs) < 2
        then error "missing output"
        else turn (outputs!!1) (direction state)
    xy2 = move direction2 (xy state)
    state2 = State { vm = vm3
                   , direction = direction2
                   , xy = xy2
                   , paintedPanels = paintedPanels2
                   }


runRobot :: Int->State->IO State
runRobot counter state =
    if VM9.halted (vm state)
    then return state
    else (stepRobot state) >>= runRobot (counter+1)
 

makeState :: String->Int->State
makeState input initialTileColorCode =
  State { vm = VM9.makeVM (readTape input) []
        , direction = DirUp
        , xy = (0,0)
        , paintedPanels = Map.singleton 0 $ Map.singleton 0 initialTileColorCode
        }


main :: IO()
main = do
    input <- readFile "day11input"
    state1 <- runRobot 0 (makeState input blackPaintCode)
    -- clear
    -- draw state1
    putStrLn 
      $ ("part 1: " ++)
      $ show
      $ sum
      $ map (Map.size . snd)
      $ Map.toList $ paintedPanels state1
    state2 <- runRobot 0 (makeState input whitePaintCode)
    putStrLn "part 2: "
    draw state2