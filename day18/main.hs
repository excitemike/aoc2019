import Control.Concurrent
import Control.Monad
import Data.Array
import Data.Char
import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import System.IO

type XY = (Int,Int)
type Level = Map.Map XY Char
type WhereTable = Map.Map Char XY
type Distances = Map.Map Char Int
type DistanceTableRow = Map.Map Char Int
type DistanceTable = Map.Map Char DistanceTableRow
type BlockedByTable = Map.Map Char [Char]

data State = State { level :: Level
                   , whereTable :: WhereTable
                   , distanceTravelled :: Int
                   , keyOrder :: String
                   , distanceTable :: DistanceTable
                   , blockedByTable :: BlockedByTable
                   } deriving(Show)


calcDistances :: Level->WhereTable->Char->DistanceTableRow
calcDistances level whereTable from = search Map.empty Set.empty [(fromXY, 0)]
  where 
    fromXY = case Map.lookup from whereTable of
               Just xy -> xy
               Nothing -> error "from not in whereTable in calcDistances"
    search :: DistanceTableRow->Set.Set XY->[(XY, Int)]->DistanceTableRow
    search mapSoFar seen [] = mapSoFar
    search mapSoFar seen ((xy, dist):queue) = 
        let { enqueue = map (\xy->(xy, dist+1))
                        $ filter (\x->Set.notMember x seen)
                        $ neighbors level xy
            ; queue' = queue ++ enqueue
            ; seen' = Set.union seen $ Set.fromList $ map fst enqueue
            ; mapSoFar' = case Map.lookup xy level of
                            Nothing -> error "invalid neighbor in calcDistances"
                            Just c | isLower c -> Map.insert c dist mapSoFar
                            Just '@'           -> Map.insert '@' dist mapSoFar
                            _                  -> mapSoFar
            }
        in search mapSoFar' seen' queue'


calcDistanceTable :: State->State
calcDistanceTable s@State{level=level,whereTable=whereTable,distanceTravelled=distanceTravelled,keyOrder=keyOrder}
    = s{distanceTable=distanceTable}
  where distanceTable = Map.fromList $ map (\c->(c, calcDistances level whereTable c)) $ "@abcdefghijklmnopqrstuvwxyz"


parse :: String->(Int,Int)->Level->WhereTable->State
parse [] (x,y) level whereTable = State { level=level
                                        , whereTable=whereTable
                                        , distanceTravelled=0
                                        , keyOrder=[]
                                        , distanceTable=Map.empty
                                        , blockedByTable=Map.fromList [('b',"fnsy"),('c',"f"),('f',"kqz"),('g',"ejw"),('h',"i"),('k',"ejw"),('l',"d"),('m',"a"),('n',"ckqtu"),('o',"bfnsy"),('p',"q"),('r',"bfnosy"),('s',"fny"),('t',"fy"),('u',"fy"),('v',"a"),('w',"a"),('x',"ejw"),('y',"ckq")]{-adejihlqmvwzkgxfcyptunsbor-}
                                        }
parse (c:s) (x,y) level whereTable = 
    parse s (x',y') level' whereTable'
  where x' = case c of
              '\n'  -> 0
              _     -> (x+1)
        y' = case c of
                '\n' -> (y+1)
                _    -> y
        level' = Map.insert (x,y) c level
        whereTable' = if (isLower c) || (c=='@')
                      then Map.insert c (x,y) whereTable
                      else whereTable


neighbors :: Level->XY->[XY]
neighbors level (x,y) =
    concat $ map f [ (x-1,y)
                   , (x+1,y)
                   , (x,y-1)
                   , (x,y+1)
                   ]
  where f xy = case Map.lookup xy level of
                    Just '@'           -> [xy]
                    Just '.'           -> [xy]
                    Just c | isAlpha c -> [xy]
                    otherwise          -> []


insertMany :: Ord k => [k]->v->Map.Map k v->Map.Map k v
insertMany [] _ table = table
insertMany (key:keys) value table = Map.insert key value $ (insertMany keys value table)


reachable :: State->Char->Distances
reachable s@State{level=level,whereTable=whereTable,keyOrder=keyOrder,distanceTable=distanceTable,blockedByTable=blockedByTable}
          fromKey
    = Map.mapWithKey (\k v->distanceTo k)
      $ Map.filterWithKey (\k v->isUnblocked k)
      $ Map.filterWithKey (\k v->not $ hasKey k)
      $ Map.filterWithKey (\k v->isLower k)
      $ whereTable
  where
    isUnblocked k = case Map.lookup k blockedByTable of
                      Nothing -> True
                      Just keys -> all hasKey keys
    hasKey k = Nothing /= elemIndex k keyOrder
    distanceTo k = case Map.lookup fromKey distanceTable of 
                     Just row -> case Map.lookup k row of
                                       Just n -> n
                                       Nothing -> error ("bad distanceTo case "++(show fromKey)++" -> "++(show k))
                     Nothing -> error "bad distanceTo case"


translateForDisplay :: Maybe Char->String
translateForDisplay (Nothing) = "\n"
translateForDisplay (Just '.') = "  "
translateForDisplay (Just '#') = "██"
translateForDisplay (Just '\n') = "\n"
translateForDisplay (Just c) = [c,c]


draw :: XY->Level->String
draw xy@(x,y) level = case translateForDisplay $ Map.lookup xy level of 
                        "\n" | x == 0    -> "\n"
                             | otherwise -> "\n"++(draw (0,y+1) level)
                        s                -> s++(draw (x+1,y) level)


argmin :: Distances->(Char, Int)
argmin theMap = foldl f init $ Map.toList theMap
   where init = ('!', (maxBound::Int))
         f a@(key1, val1) b@(key2, val2) = if (val1<val2) then a else b


moveToKey :: State->(Char, Int)->State
moveToKey s@State{distanceTravelled=distanceTravelled,keyOrder=keyOrder,distanceTable=distanceTable}
          (key, additionalDistance)
          = s { distanceTravelled = distanceTravelled+additionalDistance
              , keyOrder=keyOrder++[key]
              }


pathLengths :: State->[(String, Int)]
pathLengths s@State{distanceTravelled=distanceTravelled,keyOrder=keyOrder,blockedByTable=blockedByTable}
    | distanceTravelled > maxDist
    = []
    | (distanceTravelled + (26 - (length keyOrder)) * 12) > maxDist
    = []
    | null distances
    = [(keyOrder, distanceTravelled)]
    | null subStates
    = [(keyOrder, distanceTravelled)]
    | otherwise
    = concat $ map pathLengths subStates
  where maxDist = 5288
        distances = reachable s prev
        prev = if null keyOrder then '@' else last keyOrder
        subStates = case prev of
                      'b' -> forceChoice 'o'
                      'e' -> forceChoice 'j'
                      'k' -> forceChoice 'g'
                      'g' -> forceChoice 'x'
                      'm' -> forceChoice 'v'
                      'o' -> forceChoice 'r'
                      's' -> forceChoice 'b'
                      't' -> forceChoice 'u'
                      'v' -> forceChoice 'w'
                      _ -> map (moveToKey s) $ filter (\(key,_)->subStateFilter key) $ Map.toList distances
        forceChoice key = [moveToKey s (key, (case Map.lookup key distances of Just n -> n ; Nothing -> error ("error with forcing "++(show key)++" "++(show distances))))]
        subStateFilter key = ( (Nothing == elemIndex key "ojgxrvbuw")
                             && ((key/='b')||(Nothing /= elemIndex 's' keyOrder))
                             && ((key/='i')||(Nothing /= elemIndex 'a' keyOrder))
                             && ((key/='o')||(Nothing /= elemIndex 'b' keyOrder))
                             && ((key/='q')||(Nothing /= elemIndex 'd' keyOrder))
                             && ((key/='r')||(Nothing /= elemIndex 'o' keyOrder))
                             && ((key/='v')||(Nothing /= elemIndex 'm' keyOrder))
                             && ((key/='v')||(Nothing /= elemIndex 'w' keyOrder))
                             && ((key/='z')||(Nothing /= elemIndex 'q' keyOrder))
                             )

main :: IO()
main = do
    input <- readFile "day18input"
    let s0 = calcDistanceTable
             $ parse input (0,0) Map.empty Map.empty
    let p0 = pathLengths s0
    putStrLn $ ("pathLengths: " ++) $ unlines $ map show p0
    let p1 = foldl foldFn ("", (maxBound::Int)) $ p0
    putStrLn $ "p1:" ++ (show p1)
  where 
    foldFn :: (String,Int)->(String,Int)->(String,Int)
    foldFn a b = if (snd a)<(snd b) then a else b

















