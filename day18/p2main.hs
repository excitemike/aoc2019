import Control.Concurrent
import Control.Monad
import Data.Array
import Data.Char
import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import System.IO

type XY = (Int,Int)
type Level = Map.Map XY Char
type WhereTable = Map.Map Char XY
type Distances = [(Char, Int)]
type DistanceTableRow = Map.Map Char Int
type DistanceTable = Map.Map Char DistanceTableRow
type BlockedByTable = Map.Map Char [Char]

data State = State { level :: Level
                   , whereTable :: WhereTable
                   , distanceTravelled :: Int
                   , keyOrder :: String
                   , distanceTable :: DistanceTable
                   , blockedByTable :: BlockedByTable
                   , lastKeys :: (Char, Char, Char, Char)
                   } deriving(Show)


calcDistances :: Level->WhereTable->Char->DistanceTableRow
calcDistances level whereTable from = search Map.empty Set.empty [(fromXY, 0)]
  where 
    fromXY = case Map.lookup from whereTable of
               Just xy -> xy
               Nothing -> error $ "("++(show from)++") not in whereTable in calcDistances"
    search :: DistanceTableRow->Set.Set XY->[(XY, Int)]->DistanceTableRow
    search mapSoFar seen [] = mapSoFar
    search mapSoFar seen ((xy, dist):queue) = 
        let { enqueue = map (\xy->(xy, dist+1))
                        $ filter (\x->Set.notMember x seen)
                        $ neighbors level xy
            ; queue' = queue ++ enqueue
            ; seen' = Set.union seen $ Set.fromList $ map fst enqueue
            ; mapSoFar' = case Map.lookup xy level of
                            Nothing -> error "invalid neighbor in calcDistances"
                            Just c | isLower c -> Map.insert c dist mapSoFar
                            Just c | isDigit c -> Map.insert c dist mapSoFar
                            _                  -> mapSoFar
            }
        in search mapSoFar' seen' queue'


calcDistanceTable :: State->State
calcDistanceTable s@State{level=level,whereTable=whereTable,distanceTravelled=distanceTravelled,keyOrder=keyOrder}
    = s{distanceTable=distanceTable}
  where distanceTable = Map.fromList $ map (\c->(c, calcDistances level whereTable c)) $ "1234abcdefghijklmnopqrstuvwxyz"


parse :: String->(Int,Int)->Level->WhereTable->State
parse [] (x,y) level whereTable = State { level=level
                                        , whereTable=whereTable
                                        , distanceTravelled=0
                                        , keyOrder=[]
                                        , distanceTable=Map.empty
                                        , blockedByTable=Map.fromList [('b',"fnsy"),('c',"f"),('f',"kqz"),('g',"ejw"),('h',"i"),('k',"ejw"),('l',"d"),('m',"a"),('n',"ckqtu"),('o',"bfnsy"),('p',"q"),('r',"bfnosy"),('s',"fny"),('t',"fy"),('u',"fy"),('v',"a"),('w',"a"),('x',"ejw"),('y',"ckq")]
                                        , lastKeys=('1','2','3','4')
                                        }
parse (c:s) (x,y) level whereTable = 
    parse s (x',y') level' whereTable'
  where x' = case c of
              '\n'  -> 0
              _     -> (x+1)
        y' = case c of
                '\n' -> (y+1)
                _    -> y
        level' = Map.insert (x,y) c level
        whereTable' = if (isLower c) || (isDigit c)
                      then Map.insert c (x,y) whereTable
                      else whereTable


neighbors :: Level->XY->[XY]
neighbors level (x,y) =
    concat $ map f [ (x-1,y)
                   , (x+1,y)
                   , (x,y-1)
                   , (x,y+1)
                   ]
  where f xy = case Map.lookup xy level of
                    Just '.'           -> [xy]
                    Just c | isAlpha c -> [xy]
                    Just c | isDigit c -> [xy]
                    otherwise          -> []


insertMany :: Ord k => [k]->v->Map.Map k v->Map.Map k v
insertMany [] _ table = table
insertMany (key:keys) value table = Map.insert key value $ (insertMany keys value table)


reachable :: State->Char->Distances
reachable s@State{level=level,keyOrder=keyOrder,distanceTable=distanceTable,blockedByTable=blockedByTable}
          fromKey
    = case Map.lookup fromKey distanceTable of
        Nothing -> []
        Just distancesRow -> Map.toList
                               $ Map.mapWithKey (\k v->distanceTo k)
                               $ Map.filterWithKey (\k v->isUnblocked k)
                               $ Map.filterWithKey (\k v->not $ hasKey k)
                               $ Map.filterWithKey (\k v->isLower k)
                               $ distancesRow
  where
    isUnblocked k = case Map.lookup k blockedByTable of
                      Nothing -> True
                      Just keys -> all hasKey keys
    hasKey k = Nothing /= elemIndex k keyOrder
    distanceTo k = case Map.lookup fromKey distanceTable of 
                     Just row -> case Map.lookup k row of
                                       Just n -> n
                                       Nothing -> 1000000
                     Nothing -> 1000000


translateForDisplay :: Maybe Char->String
translateForDisplay (Nothing) = "\n"
translateForDisplay (Just '.') = "  "
translateForDisplay (Just '#') = "██"
translateForDisplay (Just '\n') = "\n"
translateForDisplay (Just c) = [c,c]


draw :: XY->Level->String
draw xy@(x,y) level = case translateForDisplay $ Map.lookup xy level of 
                        "\n" | x == 0    -> "\n"
                             | otherwise -> "\n"++(draw (0,y+1) level)
                        s                -> s++(draw (x+1,y) level)


moveToKey :: State->(Char, (Char, Int))->State
moveToKey s@State{distanceTravelled=distanceTravelled,keyOrder=keyOrder,distanceTable=distanceTable,lastKeys=(from0,from1,from2,from3)}
          (fromKey, (toKey, additionalDistance))
          = s { distanceTravelled = distanceTravelled+additionalDistance
              , keyOrder=keyOrder++[toKey]
              , lastKeys=( if from0==fromKey then toKey else from0
                         , if from1==fromKey then toKey else from1
                         , if from2==fromKey then toKey else from2
                         , if from3==fromKey then toKey else from3
                         )
              }


pathLengths :: State->[(String, Int)]
pathLengths s@State{distanceTravelled=distanceTravelled,keyOrder=keyOrder,lastKeys=(from0,from1,from2,from3),distanceTable=distanceTable}
    | distanceTravelled > maxDist
    = []
    | (distanceTravelled + (26 - (length keyOrder)) * 12) > maxDist
    = []
    | null subStates
    = [(keyOrder, distanceTravelled)]
    | otherwise
    = concat $ map pathLengths subStates
  where maxDist = 2082
        distances0 = map (\v->(from0,v)) $ reachable s from0
        distances1 = map (\v->(from1,v))
                     $ reachable s from1
        distances2 = map (\v->(from2,v)) $ reachable s from2
        distances3 = map (\v->(from3,v)) $ reachable s from3
        prev = if null keyOrder then '@' else last keyOrder
        forceChoice from to = case Map.lookup from distanceTable of
                                Just row -> case Map.lookup to row of
                                              Just d -> [moveToKey s (from, (to, d))]
        subStates :: [State]
        subStates 
          | distanceTravelled == 0 = forceChoice '2' 'a'
          | distanceTravelled < 26 = forceChoice '1' 'e'
          | prev=='e' = forceChoice 'e' 'j'
          | prev=='s' = forceChoice 's' 'b'
          | prev=='b' = forceChoice 'b' 'o'
          | prev=='o' = forceChoice 'o' 'r'
          | prev=='k' = forceChoice 'k' 'g'
          | prev=='g' = forceChoice 'g' 'x'
          | prev=='d' = forceChoice 'd' 'q'
          | prev=='q' = forceChoice 'q' 'z'
          | otherwise = map (moveToKey s)
                        $ filter (\(from,(to,d))->subStateFilter to)
                        $ concat
                        $ [distances0,distances1,distances2,distances3]
        subStateFilter key = (Nothing == elemIndex key "jborgxqz")


main :: IO()
main = do
    input <- readFile "day18inputp2"
    let s0 = calcDistanceTable
             $ parse input (0,0) Map.empty Map.empty
    let p0 = pathLengths s0
    putStrLn $ ("pathLengths: " ++) $ unlines $ map show p0
    let p2 = foldl foldFn ("", (maxBound::Int)) $ p0
    putStrLn $ "p2:" ++ (show p2)
  where 
    foldFn :: (String,Int)->(String,Int)->(String,Int)
    foldFn a b = if (snd a)<(snd b) then a else b

















