module Main (main) where

import Data.Char
import qualified Data.Map.Strict as Map
import Debug.Trace
import System.IO


type Rule = (Int, [Ingredient])
type RecipeTable = Map.Map String Rule
type Needs = Map.Map String Int


data Ingredient = Ingredient Int String
    deriving(Show)


parseIngredients :: String->([Ingredient], String)
parseIngredients s =
    case c of
        ' ' -> let ('=':'>':' ':s4) = s3
               in ([i], s4)
        ',' -> let { (' ':s4) = s3
                   ; (ingredients, s5) = parseIngredients s4
                   }
               in (i:ingredients, s5)
  where (count, (_:s2)) = span isDigit s
        (name, (c:s3)) = span isAlpha s2 
        i = Ingredient (read count) name


parseLine :: String->(String, (Int, [Ingredient]))
parseLine s = (productName, ((read productCount), ingredients))
  where (ingredients, s2) = parseIngredients s
        (productCount, (_:productName)) = span isDigit s2


timesToRunRuleNoExtra :: String->RecipeTable->Int->Int
timesToRunRuleNoExtra name recipeTable needCount = 
    case Map.lookup name recipeTable of
      Just (productCount, ingredients) ->
        quot needCount productCount
      Nothing -> 0


addNeeds :: [Ingredient]->Int->Needs->Needs
addNeeds ingredients times needs =
    case ingredients of
      [] -> needs
      (i:rest) -> needs3 i rest
  where unMaybe Nothing = 0
        unMaybe (Just x) = x
        needs2 icount iname
          = Map.alter 
              (\x->Just (unMaybe x + (icount*times)))
              iname
              needs
        needs3 (Ingredient icount iname) rest
          = addNeeds rest times (needs2 icount iname)


applyRule :: String->Int->Rule->Int->Needs->Needs
applyRule name
          needCount
          rule@(productionCount, ingredients)
          timesToRunRule
          needs =
    addNeeds ingredients timesToRunRule needs2
  where remaining = (needCount - timesToRunRule*productionCount)
        needs2 = Map.insert name remaining needs


exactlyMeetableNeeds :: RecipeTable->Needs->[(String,Int)]
exactlyMeetableNeeds recipeTable needs =
    filter filterFunc 
      $ map times
      $ (Map.toList needs)
  where times :: (String,Int)->(String, Int)
        times (name, needCount) = 
          ( name
          , timesToRunRuleNoExtra name recipeTable needCount
          )
        filterFunc :: (String,Int)->Bool
        filterFunc = (\ (name, count) -> (name/="ORE")&&(count>0))


meetNeedsExact :: RecipeTable->Needs->Needs
meetNeedsExact recipeTable needs =
    case exactlyMeetableNeeds recipeTable needs of
      [] -> needs
      ((name, timesToRunRule):_) -> 
        case Map.lookup name needs of
          Nothing -> error "some mistake in meetNeedsExact"
          Just needCount -> 
            case Map.lookup name recipeTable of 
              Nothing -> error ("don't know how to make " ++ name)
              Just rule ->
                meetNeedsExact recipeTable
                  $ applyRule name needCount rule timesToRunRule needs


meetNeedWithExtra :: RecipeTable->Needs->Needs
meetNeedWithExtra recipes needs = 
    case nextNeeds of
      [] -> needs
      ((name, needCount):_) -> 
        meetNeedsExact recipes
        $ applyRule
            name
            needCount
            (rule name)
            (times name needCount)
            needs
  where rule name = case Map.lookup name recipes of
                      Just r -> r
                      Nothing -> error ("no rule to make " ++ name)
        times name needCount =
          case Map.lookup name recipes of
            Just (productCount, ingredients)
              -> quot 
                   (needCount+productCount-1)
                   productCount
            Nothing -> 0
        nextNeeds = 
          filter (\(name, count) -> ((name/="ORE") && (count>0)))
          $ Map.toList
          $ needs


meetSpecificNeedWithExtra :: String->RecipeTable->Needs->Needs
meetSpecificNeedWithExtra chemical recipes needs = 
    case Map.lookup chemical needs of
      Nothing -> needs
      Just needCount -> 
        meetNeedsExact recipes
        $ applyRule
            chemical
            needCount
            (rule chemical)
            (times chemical needCount)
            needs
  where rule name = case Map.lookup name recipes of
                      Just r -> r
                      Nothing -> error ("no rule to make " ++ name)
        times name needCount =
          case Map.lookup name recipes of
            Just (productCount, ingredients)
              -> quot 
                   (needCount+productCount-1)
                   productCount
            Nothing -> 0

          
argmax :: Ord b => (a->b)->[a]->a
argmax _ [] = error ":("
argmax f (x:xs') = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'


p2 :: Int->String->String
p2 fuel input = case Map.lookup "ORE" finalNeeds of
    Just x -> ("p2: "++(show fuel)++" FUEL is produced by "++(show x)++" ORE")
    _ -> error "!?"
  where needs = meetNeedsExact recipes $ Map.fromList [("FUEL", fuel)]
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        process needs = case Map.size $ Map.filter (>0) needs of
            1 -> needs
            _ -> process $ meetNeedWithExtra recipes needs
        finalNeeds = Map.filter (>0) $ process needs


p1 :: String->Int
p1 input = case Map.lookup "ORE" finalNeeds of
    Just x -> x
    _ -> error "!?"
  where needs = meetNeedsExact recipes $ Map.fromList [("FUEL", 1)]
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        process needs = case Map.size $ Map.filter (>0) needs of
            1 -> needs
            _ -> process $ meetNeedWithExtra recipes needs
        finalNeeds = Map.filter (>0) $ process needs


ex5 :: Bool
ex5 = case Map.lookup "ORE" finalNeeds of
    Just 2210736 -> True
    _ -> False
  where needs = meetNeedsExact recipes $ Map.fromList [("FUEL", 1)]
        input = "171 ORE => 8 CNZTR\n7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\n114 ORE => 4 BHXH\n14 VRPVC => 6 BMBT\n6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\n6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\n15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\n13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\n5 BMBT => 4 WPTQ\n189 ORE => 9 KTJDG\n1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\n12 VRPVC, 27 CNZTR => 2 XDBXC\n15 KTJDG, 12 BHXH => 5 XCVML\n3 BHXH, 2 VRPVC => 7 MZWV\n121 ORE => 7 VRPVC\n7 XCVML => 6 RJRHP\n5 BHXH, 4 VRPVC => 5 LTCX"
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        process needs = case Map.size $ Map.filter (>0) needs of
            1 -> needs
            _ -> process $ meetNeedWithExtra recipes needs
        finalNeeds = Map.filter (>0) $ process needs


ex4 :: Bool
ex4 = case Map.lookup "ORE" finalNeeds of
    Just 180697 -> True
    _ -> False
  where needs = meetNeedsExact recipes $ Map.fromList [("FUEL", 1)]
        input = "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n17 NVRVD, 3 JNWZP => 8 VPVL\n53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n22 VJHF, 37 MNCFX => 5 FWMGM\n139 ORE => 4 NVRVD\n144 ORE => 7 JNWZP\n5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n145 ORE => 6 MNCFX\n1 NVRVD => 8 CXFTF\n1 VJHF, 6 MNCFX => 4 RFSQX\n176 ORE => 6 VJHF"
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        process needs = case Map.size $ Map.filter (>0) needs of
            1 -> needs
            _ -> process $ meetNeedWithExtra recipes needs
        finalNeeds = Map.filter (>0) $ process needs


ex3 :: Bool
ex3 = case Map.lookup "ORE" finalNeeds of
    Just 13312 -> True
    _ -> False
  where needsInit = Map.fromList [("FUEL", 1)]
        input = "157 ORE => 5 NZVS\n165 ORE => 6 DCFZ\n44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n179 ORE => 7 PSHF\n177 ORE => 5 HKGWZ\n7 DCFZ, 7 PSHF => 2 XJWVT\n165 ORE => 2 GPVTF\n3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        finalNeeds = Map.filter (>0)
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedsExact recipes needsInit


ex2 :: Bool
ex2 = case Map.lookup "ORE" finalNeeds of
    Just 165 -> True
    _ -> False
  where needsInit = Map.fromList [("FUEL", 1)]
        input = "9 ORE => 2 A\n8 ORE => 3 B\n7 ORE => 5 C\n3 A, 4 B => 1 AB\n5 B, 7 C => 1 BC\n4 C, 1 A => 1 CA\n2 AB, 3 BC, 4 CA => 1 FUEL"
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        finalNeeds = Map.filter (/=0)
                      $ meetNeedWithExtra recipes
                      $ meetNeedWithExtra recipes
                      $ meetNeedsExact recipes needsInit


ex1 :: Bool
ex1 = case Map.lookup "ORE" finalNeeds of
    Just 31 -> True
    _ -> False
  where needsInit = Map.fromList [("FUEL", 1)]
        input = "10 ORE => 10 A\n1 ORE => 1 B\n7 A, 1 B => 1 C\n7 A, 1 C => 1 D\n7 A, 1 D => 1 E\n7 A, 1 E => 1 FUEL"
        recipes = Map.fromList
          $ map parseLine
          $ lines
          $ input
        finalNeeds = Map.filter (/=0)
                      $ meetNeedWithExtra recipes
                      $ meetNeedsExact recipes needsInit


main :: IO()
main = do
    input <- readFile "day14input"
    putStrLn ""
    putStrLn $ ("ex1: "++) $ show ex1
    putStrLn $ ("ex2: "++) $ show ex2
    putStrLn $ ("ex3: "++) $ show ex3
    putStrLn $ ("ex4: "++) $ show ex4
    putStrLn $ ("ex5: "++) $ show ex5
    putStrLn $ ("p1: "++) $ show $ p1 input
    putStrLn
      $ unlines 
      $ map (\x-> p2 x input)
      $ [2144701..2144705]
    putStrLn ""
  
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      