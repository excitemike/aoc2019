module Main (main) where

import Data.Bits
import Data.Char
import Graphics.Gloss
import qualified Data.Map.Strict as Map
import Debug.Trace
import Numeric
import System.IO
import qualified VM9

data Thing = Empty | Wall | Oxy | Droid | Start | Atmo Int | Answer
    deriving(Eq, Show)


-- commands
data Command = Command Int
    deriving(Eq, Show)
north = Command 1
south = Command 2
west = Command 3
east = Command 4

cw :: Command->Command
cw (Command 1) = Command 4
cw (Command 2) = Command 3
cw (Command 3) = Command 1
cw (Command 4) = Command 2

ccw :: Command->Command
ccw (Command 1) = Command 3
ccw (Command 2) = Command 4
ccw (Command 3) = Command 2
ccw (Command 4) = Command 1

-- status code
data Status = Status Int
hitWall = Status 0
oneStep = Status 1
oneStepFoundOxy = Status 2


data SimState = SimState
    { robot :: XY
    , lastCommand :: Command
    , shipMap :: TileMap
    , program :: VM9.VM
    , p1Done :: Bool
    , p2Done :: Bool
    }
    deriving(Show)

window = InWindow "Day15" (800, 800) (10, 10)

colorAt :: (Int,Int)->Color
colorAt (x,y)
    | i==0 = dark $ dark rose
    | i==1 = dark $ dark violet
    | i==2 = dark $ dark azure
    | i==3 = dark $ dark aquamarine
    | i==4 = dark $ dark chartreuse
    | i==5 = dark $ dark orange
  where i = mod (x*2+y+(quot x 3)) 6

type XY = (Int,Int)
type TileMap = Map.Map XY Thing

          
argmax :: Ord b => (a->b)->[a]->a
argmax _ [] = error ":("
argmax f (x:xs') = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s


update :: a->Float->SimState->SimState
update _ _ state | not $ p1Done state = stepGame32 state
                 | not $ p2Done state = stepGame state
                 | otherwise = state
  where stepGame2 = stepGame . stepGame
        stepGame4 = stepGame2 . stepGame2
        stepGame8 = stepGame4 . stepGame4
        stepGame16 = stepGame8 . stepGame8
        stepGame32 = stepGame16 . stepGame16
        stepGame64 = stepGame32 . stepGame32
        stepGame128 = stepGame64 . stepGame64
        stepGame256 = stepGame128 . stepGame128
        stepGame512 = stepGame256 . stepGame256


stepXY :: Command->XY->XY
stepXY command (x,y) = case command of
                         Command 1 -> (x, y+1)
                         Command 2 -> (x, y-1)
                         Command 3 -> (x-1, y)
                         Command 4 -> (x+1, y)
                      


stepGame :: SimState->SimState
stepGame state | VM9.halted (program state) = state
               | not $ p1Done state = p1 state
               | not $ p2Done state = p2 state
               | otherwise = state


p1 :: SimState->SimState
p1 state = state'
  where vm = program state
        vm' = case inputCommand of 
                Command code -> vm { VM9.inputs = [code] }
        (vm'', output) = VM9.runUntilBlocked vm'
        status = Status $ output!!0
        xy = robot state
        robot' = case (inputCommand, status) of
                   (_, Status 0) -> xy
                   (c, _)        -> stepXY inputCommand xy
        insXY = stepXY inputCommand xy
        insTile = case status of
                    Status 0 -> Wall
                    Status 1 -> Empty
                    Status 2 -> Oxy
        checkCommand c = case Map.lookup (stepXY c xy) (shipMap state) of
                           Just Wall -> False
                           _ -> True
        getInputCommand prevCmd =
            if checkCommand $ cw prevCmd then cw prevCmd
            else if checkCommand prevCmd then prevCmd
            else if checkCommand $ ccw prevCmd then ccw prevCmd
            else cw $ cw prevCmd
        inputCommand = getInputCommand $ lastCommand state
        shipMap' = case Map.lookup insXY (shipMap state) of 
                     Nothing -> Map.insert insXY insTile (shipMap state)
                     Just _ -> (shipMap state)
        lastCommand' = case status of
                         Status 0 -> lastCommand state
                         _ -> inputCommand
        state' = state { robot = robot'
                       , lastCommand = lastCommand' 
                       , shipMap = shipMap'
                       , program = vm''
                       , p1Done = (Just Wall)==(Map.lookup (-1,1) (shipMap state))
                       }


p2 :: SimState->SimState
p2 state = state'
  where updateFunc :: XY->Thing->Thing
        updateFunc xy thing =
          case thing of
            Empty -> spreadOxy xy thing
            _ -> thing
        spreadOxy :: XY->Thing->Thing
        spreadOxy xy thing =
            if not $ any hasAtmo $ neighbors xy
            then thing
            else Atmo 
                   $ (+1)
                   $ minimum
                   $ map timeToAtmo $ neighbors xy
        lookupThing place = 
          case Map.lookup place (shipMap state) of
            Nothing    -> Wall -- should be unreachable?
            Just thing -> thing
        neighbors xy = map ( lookupThing
                           . (flip stepXY xy)
                           . Command
                           )
                           [1..4]
        hasAtmo t = case t of
                      Oxy -> True
                      Atmo n -> True
                      _ -> False
        timeToAtmo thing =
          case thing of
            Oxy    -> 0
            Atmo n -> n
            _      -> (maxBound :: Int)
        shipMap' = Map.mapWithKey updateFunc $ shipMap state
        state' = state { shipMap = shipMap'
                       , p2Done = not $ any (==Empty) $ Map.elems shipMap'
                       }


p2Answer :: SimState->Int
p2Answer state = maximum 
               $ map atmoTime
               $ Map.elems
               $ shipMap state
  where atmoTime thing = case thing of
                           Atmo n -> n
                           _ -> -1


render :: SimState->Picture
render state =
    pictures $ map draw drawables
  where tiles = Map.toList (shipMap state)
        drawables = tiles ++ [((robot state), Droid), ((0,0), Start), ((-20, 23), Answer)]
        place (x,y) =
            translate 
              (10 * (fromIntegral x))
              (10 * (fromIntegral y))
        draw (xy, Empty) = place xy $ color white $ rectangleSolid 4 4
        draw (xy, Wall) = place xy $ color black $ rectangleWire 8 8
        draw (xy, Oxy) = place xy $ color blue $ circleSolid 5
        draw (xy, Droid) = place xy $ color red $ circle 5
        draw (xy, Start) = place xy $ color white $ circle 5
        draw (xy, Atmo n) = place xy $ color (atmoColor n) $ rectangleSolid 10 10
        draw (xy, Answer) = place xy $ scale 0.25 0.25 $ color black $ text $ show $ p2Answer state
        -- atmoColor n = [rose,violet,azure,aquamarine,chartreuse,orange] !! (mod n 6)
        atmoPct n = (fromIntegral n) / (fromIntegral $ p2Answer state)
        square x = x * x
        atmoColor n = let t = square $ square $ atmoPct n in makeColor (2.0 * (t - (square t))) (square $ square t) (1.0 - t) 1.0


main :: IO()
main = do
    input <- readFile "day15input"
    simulate
      window
      bgColor
      frameRate
      SimState { robot = (0,0)
               , lastCommand = Command 1
               , shipMap = Map.singleton (0,0) Empty
               , program = makeVm input
               , p1Done = False
               , p2Done = False
               }
      render
      update
  where makeVm = (\tapeInit -> VM9.makeVM tapeInit []) . readTape
        frameRate = 30
        bgColor = (greyN 0.35)
  
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      