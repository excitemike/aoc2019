module Main (main) where

import qualified Data.Map.Strict as Map
import Graphics.Gloss
import System.IO

import qualified VM9

type Screen = Map.Map (Int,Int) Int
type GameState = (Screen, VM9.VM)

width = 400
height = 400
bgColor = greyN 0.30
frameRate = 120

window = InWindow "Day13" (width, height) (10, 10)


colorAt :: (Int,Int)->Color
colorAt (x,y)
    | i==0 = rose
    | i==1 = violet
    | i==2 = azure
    | i==3 = aquamarine
    | i==4 = chartreuse
    | i==5 = orange
  where i = mod (x*2+y+(quot x 3)) 6


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s
 

parseOutput :: [Int]->[((Int,Int), Int)]
parseOutput [] = []
parseOutput (x:y:tileId:rest) = ((x,y), tileId) : parseOutput rest


insertMany :: Ord k => [(k, a)] -> Map.Map k a -> Map.Map k a
insertMany [] m = m
insertMany ((key, val):rest) m = insertMany rest $ Map.insert key val m


playGame :: Screen->VM9.VM->Int
playGame screen vm | VM9.halted vm' = score
                   | otherwise  = playGame screen' (vm' { VM9.inputs = [joyStick]})
  where (vm', output) = VM9.runUntilBlocked vm
        screen' = insertMany (parseOutput output) screen
        screenAsList = Map.toList screen'
        isBall ((x,y),tileId) = (tileId==4) && (x/=(-1))
        isPaddle ((x,y),tileId) = (tileId==3) && (x/=(-1))
        ballX   = (fst . fst . (!!0) . (filter isBall)) screenAsList
        paddleX = (fst . fst . (!!0) . (filter isPaddle)) screenAsList
        joyStick | ballX < paddleX = -1
                 | ballX > paddleX = 1
                 | otherwise = 0
        score = (snd . (!!0) . filter ((==(-1)) . fst . fst)) screenAsList
        

render :: GameState->Picture
render gs@(screen, vm) =
    pictures [ shadow (rectangleSolid 10 10) walls
             , shadow (rectangleSolid 8 8) blocks
             , shadow (rectangleSolid 58 8) paddles
             , shadow (circleSolid 8) balls
             , draw (rectangleSolid 10 10) walls
             , draw (rectangleSolid 8 8) blocks
             , draw (rectangleSolid 58 8) paddles
             , draw (circleSolid 8) balls
             ]
  where screenAsList = Map.toList screen
        isTile desiredTileId ((x,y),tileId) = (tileId==desiredTileId) && (x/=(-1))
        walls = map fst $ filter (isTile 1) screenAsList
        blocks = map fst $ filter (isTile 2) screenAsList
        paddles = map fst $ filter (isTile 3) screenAsList
        balls = map fst $ filter (isTile 4) screenAsList
        place (x,y) =
            translate 
              ((-180) + 10 * (fromIntegral x))
              (140 - 10 * (fromIntegral y))
        draw shape = 
            pictures
            . (map (\xy -> place xy $ color (colorAt xy) shape))
        shadow shape =
            pictures
            . (map (\(x,y)
                    -> translate 3 (-3) 
                    $  place ((x),(y))
                    $  color black shape))


update :: a->Float->GameState->GameState
update _ _ = stepGame


stepGame :: GameState->GameState
stepGame gs@(screen, vm) | VM9.halted vm = gs
                         | otherwise = (screen', vm'')
  where (vm', output) = VM9.runUntilBlocked vm
        screen' = insertMany (parseOutput output) screen
        screenAsList = Map.toList screen'
        isBall ((x,y),tileId) = (tileId==4) && (x/=(-1))
        isPaddle ((x,y),tileId) = (tileId==3) && (x/=(-1))
        ballX   = (fst . fst . (!!0) . (filter isBall)) screenAsList
        paddleX = (fst . fst . (!!0) . (filter isPaddle)) screenAsList
        joyStick | ballX < paddleX = -1
                 | ballX > paddleX = 1
                 | otherwise = 0
        vm'' = vm' { VM9.inputs = [joyStick] }


main :: IO()
main = do
    input <- readFile "day13input"
    {-putStrLn $ p1 input
    putStrLn $ p2 input -}
    simulate
      window
      bgColor
      frameRate
      (Map.empty, (p3vm input))
      render
      update
  where fixRedraws = Map.toList . Map.fromList
        blocks = (filter ((==2) . snd))
        makeVm = (\tapeInit -> VM9.makeVM tapeInit [])
        p1 = ("part 1: " ++) . show . length . blocks . fixRedraws . parseOutput . snd . VM9.runUntilBlocked . makeVm . readTape
        freePlay = (\(a:rest) -> (2:rest))
        p2 = ("part 2: " ++) . show . (playGame Map.empty) . makeVm . freePlay . readTape
        p3vm = makeVm . freePlay . readTape
        firstScreen = Map.fromList . parseOutput . snd . VM9.runUntilBlocked . p3vm
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      