@echo off
setlocal

:start
cls

echo building with stack...
stack build

echo Running with stack...
stack run

pause
goto start

:error
echo ERROR
pause
goto start