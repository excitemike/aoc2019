module Main (main) where

import Data.Array
import Data.Bits
import Data.Char
import Graphics.Gloss
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import Numeric
import System.IO
import qualified VM9

type XY = (Int,Int)
type LabelTable = Map.Map String [XY]
type Maze = (Map.Map XY Char)
type DistanceTable = (Map.Map (XY,Int) Int)
type TpTable = (Map.Map XY XY)
type CalcDistancesFn = XY->Maze->TpTable->XY->(Int,Int)->DistanceTable

window = InWindow "AoC" (800, 800) (10, 10)

maxDepth = 1000

colorAt :: (Int,Int)->Color
colorAt (x,y)
    | i==0 = rose
    | i==1 = violet
    | i==2 = azure
    | i==3 = aquamarine
    | i==4 = chartreuse
    | i==5 = orange
  where i = mod (x*2+y+(quot x 3)) 6


render :: [(XY,Int)]->Picture
render state = pictures $ subPictures
  where place (x,y) =
            translate 
              (10 * (fromIntegral x) - 250)
              (10 * (fromIntegral y) - 250)
        draw (xy, 0) = place xy $ color (colorAt xy) $ rectangleSolid 2 2
        draw (xy, 1) = place xy $ color (colorAt xy) $ rectangleSolid 10 10
        tilePictures = map draw state
        label = place (0,-10) 
                $ scale 0.25 0.25
                $ color white
                $ text
                $ ("p1: "++)
                $ show
                $ length
                $ filter ((==1) . snd)
                $ seq state state
        subPictures = label:tilePictures


insertMany :: Ord k => [(k, XY)] -> Map.Map k [XY] -> Map.Map k [XY]
insertMany [] m = m
insertMany ((key, val):rest) m = insertMany rest $ Map.alter alterFn key m
  where
    alterFn :: (Maybe [XY])->(Maybe [XY])
    alterFn (Just prevList)   = Just (val:prevList)
    alterFn Nothing         = Just [val]


findLabels :: Maze->LabelTable->LabelTable
findLabels maze labelTableSoFar = insertMany inserts labelTableSoFar
  where
    inserts = unMaybe $ findLabelsInt $ Map.keys maze
    unMaybe [] = []
    unMaybe (x:xs) = case x of
                        Just x' -> x' : (unMaybe xs)
                        Nothing -> unMaybe xs
    findLabelsInt :: [XY]->[Maybe (String, XY)]
    findLabelsInt [] = []
    findLabelsInt (xy@(x,y):rest) = (labelCheck xy):(findLabelsInt rest)
    labelCheck :: XY->Maybe (String,XY)
    labelCheck xy = 
        case Map.lookup xy maze of
            Just '.' -> let { u1 = up 1 xy maze
                            ; u2 = up 2 xy maze
                            ; d1 = down 1 xy maze
                            ; d2 = down 2 xy maze
                            ; l1 = left 1 xy maze
                            ; l2 = left 2 xy maze
                            ; r1 = right 1 xy maze
                            ; r2 = right 2 xy maze
                            }
                        in
                            if (isUpper u1) && (isUpper u2)
                            then Just ([u2,u1], xy)
                            else if (isUpper d1) && (isUpper d2)
                            then Just ([d1,d2], xy)
                            else if (isUpper l1) && (isUpper l2)
                            then Just ([l2,l1], xy)
                            else if (isUpper r1) && (isUpper r2)
                            then Just ([r1,r2], xy)
                            else Nothing
            _        -> Nothing


up n (x,y) maze =
    case Map.lookup (x,y-n) maze of
        Just c -> c
        _      -> ' '


down n (x,y) maze =
    case Map.lookup (x,y+n) maze of
        Just c -> c
        _      -> ' '


left n (x,y) maze =
    case Map.lookup (x-n,y) maze of
        Just c -> c
        _      -> ' '


right n (x,y) maze =
    case Map.lookup (x+n,y) maze of
        Just c -> c
        _      -> ' '


parse :: String->(Int,Int)->Maze->Maze
parse [] (x,y) mazeChars = mazeChars
parse (c:rest) (x,y) mazeChars = parse rest (x',y') mazeChars'
  where x' = case c of
              '\n'  -> 0
              _     -> (x+1)
        y' = case c of
                '\n' -> (y+1)
                _    -> y
        mazeChars' = Map.insert (x,y) c mazeChars


canWalk :: Maze->XY->Bool
canWalk maze xy =
    case Map.lookup xy maze of
        Just '.' -> True
        _ -> False


neighborsNoTp :: Maze->(XY,Int)->[(XY,Int)]
neighborsNoTp maze xyd@((x,y),d) = map (\xy2->(xy2,d)) $ filter (canWalk maze) [(x,y-1),(x,y+1),(x-1,y),(x+1,y)]


calcDistancesP1 :: CalcDistancesFn
calcDistancesP1 fromXY maze tpTable endXY (mapW,mapH) =
    search Map.empty Set.empty [(fromXY, 0, 0)]
  where
    neighborsByTp :: (XY, Int)->[(XY, Int)]
    neighborsByTp xyd@(xy,d) = case Map.lookup xy tpTable of
                                Just xy2 -> [(xy2,d)]
                                Nothing   -> []
    neighbors :: (XY,Int)->[(XY,Int)]
    neighbors xyd@(xy,d) = (neighborsNoTp maze xyd) ++ (neighborsByTp xyd)
    search :: DistanceTable->Set.Set (XY, Int)->[(XY, Int, Int)]->DistanceTable
    search distancesSoFar seen [] = distancesSoFar
    search distancesSoFar seen ((xy, depth, dist):queue) = 
        let { enqueue = map (\(xy2,d2)->(xy2, d2, dist+1))
                        $ filter (\xyd->Set.notMember xyd seen)
                        $ neighbors (xy, depth)
            ; queue' = queue ++ enqueue
            ; seen' = Set.union seen $ Set.fromList $ map (\(a,b,c)->(a,b)) enqueue
            ; distancesSoFar' = Map.insert (xy,0) dist distancesSoFar
            }
        in  if (endXY==xy)
            then Map.insert (xy,0) dist distancesSoFar
            else search distancesSoFar' seen' queue'


calcDistancesP2 :: CalcDistancesFn
calcDistancesP2 fromXY maze tpTable endXY (mapW,mapH) =
    search Map.empty (Set.singleton (fromXY, 0)) [(fromXY, 0, 0)]
  where
    neighborsByTp :: (XY, Int)->[(XY, Int)]
    neighborsByTp xyd@(xy,d) = case Map.lookup xy tpTable of
                                    Just xy2 -> if teleportsInwards xyd then [(xy2, d+1)] else if d>0 then [(xy2, d-1)] else []
                                    Nothing  -> []
    neighbors :: (XY, Int)->[(XY, Int)]
    neighbors xyd = (neighborsNoTp maze xyd) ++ (neighborsByTp xyd)
    teleportsInwards ((x,y),d) = let result = (x>2)&&(x<(mapW-3))&&(y>3)&&(y<(mapH-3))
                             in result
    search :: DistanceTable->Set.Set (XY, Int)->[(XY, Int, Int)]->DistanceTable
    search distancesSoFar seen [] = distancesSoFar
    search distancesSoFar seen ((xy, depth, dist):queue) = 
        let { enqueue1 = map (\(xy, depth)->(xy, depth, dist+1))
                         $ filter (\xyd->Set.notMember xyd seen)
                         $ neighborsNoTp maze (xy, depth)
            ; enqueue2 = map (\(xy, depth)->(xy, depth, dist+1))
                         $ filter (\xyd->Set.notMember xyd seen)
                         $ neighborsByTp (xy, depth)
            ; enqueue = enqueue1 ++ enqueue2
            ; queue' = queue ++ enqueue
            ; seen' = Set.union seen $ Set.fromList $ map (\(a,b,c)->(a,b)) enqueue
            ; distancesSoFar' = Map.insert (xy, depth) dist distancesSoFar
            }
        in  if (endXY==xy)&&(0==depth)
            then trace ("reached "++(show (xy,depth,dist))) $ distancesSoFar'
            else if depth <= maxDepth
            then search distancesSoFar' seen' queue'
            else error $ "maxDepth exceeded " ++ (show depth)


findDistance :: String->CalcDistancesFn->IO Int
findDistance s calcDistancesFn = do
    putStrLn $ "starting from " ++ (show startXY)
    putStrLn $ "map size: " ++ (show (mapW,mapH))
    return distance
  where 
    maze = parse s (0,0) Map.empty
    labelTable = findLabels maze Map.empty
    tpTable :: TpTable
    tpTable = Map.fromList 
              $ concat
              $ map (\(a:b:_)->[(a,b),(b,a)])
              $ filter (\x->2==(length x))
              $ map snd
              $ Map.toList labelTable
    startXY = case Map.lookup "AA" labelTable of
                Just (xy:_) -> xy
                Nothing -> error "no AA in label table"
    endXY   = case Map.lookup "ZZ" labelTable of
                Just (xy:_) -> xy
                Nothing -> error "no ZZ in label table"
    mapW    = maximum $ map fst $ Map.keys maze
    mapH    = maximum $ map snd $ Map.keys maze
    distances = calcDistancesFn startXY maze tpTable endXY (mapW,mapH)
    distance = case Map.lookup (endXY,0) distances of
                Just n -> n
                _ -> error $ "no distance stored for ZZ/"++(show endXY)

main :: IO()
main = do
    input <- readFile "input"
    putStrLn ""
    exInput <- readFile "exampleinput"
    p1ExResult <- findDistance exInput calcDistancesP1
    putStrLn 
      $ ("p1 example (should be 58): "++)
      $ show
      $ p1ExResult
    p1Result <- findDistance input calcDistancesP1
    putStrLn 
      $ ("p1 (should be 490): "++)
      $ show
      $ p1Result
    p2ExInput <- readFile "exampleinput2"
    p2ExResult <- findDistance p2ExInput calcDistancesP2
    putStrLn 
      $ ("p2 example: (should be 396) "++)
      $ show
      $ p2ExResult
    p2Result <- findDistance input calcDistancesP2
    putStrLn 
      $ ("p2: "++)
      $ show
      $ p2Result
      
      
      
      
      
      
      
      
      
      
      
      
      