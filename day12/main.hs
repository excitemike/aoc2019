import Control.Concurrent
import Control.Monad
import Data.Array
import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import System.IO

type Moon = (Vec3, Vec3)
type Vec3 = (Int, Int, Int)


moonsInit = [ ((-3,10,-1),(0,0,0))
            , ((-12,-10,-5),(0,0,0))
            , ((-9,0,10),(0,0,0))
            , ((7,-5,-3),(0,0,0))
            ]


testCase1 = [ ((-1,0,2),(0,0,0))
            , ((2,-10,-7),(0,0,0))
            , ((4,-8,8),(0,0,0))
            , ((3,5,-1),(0,0,0))
            ]

testCase2 = [ ((-8, -10,  0),(0,0,0))
            , (( 5,  5, 10),(0,0,0))
            , (( 2, -7,  3),(0,0,0))
            , (( 9, -8, -3),(0,0,0))
            ]

sign :: Int->Int
sign x | x < 0     = -1
       | x > 0     = 1
       | otherwise = 0

deltaV :: Moon->Moon->Vec3
deltaV ((x1,y1,z1),v1) ((x2,y2,z2),v2) =
    ( sign (x2-x1)
    , sign (y2-y1)
    , sign (z2-z1)
    )


vecAdd :: Vec3->Vec3->Vec3
vecAdd (x1,y1,z1) (x2,y2,z2) = (x1+x2,y1+y2,z1+z2)


applyGravity :: Moon->[Moon]->Moon
applyGravity moon otherMoons =
  (fst moon,
   foldl vecAdd (snd moon)
    $ map (deltaV moon)
    $ otherMoons
  )


applyVelocity :: Moon->Moon
applyVelocity moon =
  ( vecAdd (fst moon) (snd moon)
  , snd moon
  )


stepSystem :: [Moon]->[Moon]
stepSystem moons =
  map ( applyVelocity
      . (\moon -> applyGravity moon (otherMoons moon))
      )
      moons
  where
    otherMoons moon = [ other | other <- moons, moon /= other ]


applyNTimes :: Int->([Moon]->[Moon])->[Moon]->[Moon]
applyNTimes n f moons
  | n < 1     = moons
  | otherwise = applyNTimes (n-1) f (f moons)


searchForRepeat :: ([Moon]->[(Int,Int)])->[Moon]->Int->[(Int,Int)]->Int
searchForRepeat f moons turnNumber searchState =
    if (f moons') == searchState
    then turnNumber'
    else searchForRepeat f moons' turnNumber' searchState
  where
    moons' = stepSystem moons
    turnNumber' = turnNumber + 1


main :: IO()
main = do
    putStrLn 
      $ ("test case 1 after 28 steps:\n"++)
      $ unlines
      $ map show
      $ applyNTimes 28 stepSystem
      $ testCase1
    putStrLn 
      $ ("part 1: "++)
      $ show
      $ sum
      $ map (totalEnergy)
      $ applyNTimes 1000 stepSystem
      $ moonsInit
    putStrLn ("xs repeat after " ++ (show xPeriod) ++ " steps")
    putStrLn ("ys repeat after " ++ (show yPeriod) ++ " steps")
    putStrLn ("zs repeat after " ++ (show zPeriod) ++ " steps")
    putStrLn $ "part 2: " ++ (show $ lcm xPeriod $ lcm yPeriod zPeriod)
  where
    potentialEnergy ((x,y,z),_) = sum $ (map abs) [x,y,z]
    kineticEnergy (_,(x,y,z)) = sum $ (map abs) [x,y,z]
    totalEnergy moon = (potentialEnergy moon) * (kineticEnergy moon)
    getXs = (map (\((x,y,z),(vx,vy,vz)) -> (x,vx)))
    getYs = (map (\((x,y,z),(vx,vy,vz)) -> (y,vy)))
    getZs = (map (\((x,y,z),(vx,vy,vz)) -> (z,vz)))
    xPeriod = searchForRepeat getXs moonsInit 0 (getXs moonsInit)
    yPeriod = searchForRepeat getYs moonsInit 0 (getYs moonsInit)
    zPeriod = searchForRepeat getZs moonsInit 0 (getZs moonsInit)