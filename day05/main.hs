import Data.Char
import System.IO 
import Control.Monad
import Text.ParserCombinators.ReadP
import qualified VM5

eat_comma :: String->String
eat_comma s = 
  if (null s)
  then s
  else let c = s !! 0
    in if (c==',')
    then (drop 1 s)
    else s

read_char c s =
  if null s
  then (s, s)
  else let c_test = s !! 0
    in if (c_test==c)
    then (c : [], drop 1 s)
    else ([], s)

read_comma_separated_ints :: String->[Int]
read_comma_separated_ints s = 
  if (null s)
  then []
  else let (i, s2) = (read_int s)
    in (i : (read_comma_separated_ints $ eat_comma s2))

read_digits :: String->(String,String)
read_digits s = 
  if (null s)
  then ("","")
  else let c = s !! 0
    in
      if (isDigit c)
      then let (c2, s2) = (read_digits (drop 1 s))
        in (c : c2, s2)
      else ("", s)

read_int :: String->(Int,String)
read_int s = let {
    (s1, s2) = read_char '-' s;
    (s3, s4) = read_digits s2;}
  in
    ((read::String->Int) (s1++s3), s4)


make_vm :: [Int]->Int->VM5.VM
make_vm tape_init input =
  VM5.VM
    { VM5.program_counter = 0
    , VM5.tape = tape_init
    , VM5.inputs = input : []
    , VM5.outputs = []
    }


test :: [Int]->Int->Int->Bool
test tape_init input expected =
  let 
    { vm = make_vm tape_init input
    ; vm' = VM5.runVM vm
    ; outputs = VM5.outputs vm'
    ; final_output = outputs !! 0
    }
  in expected == final_output


tests :: IO()
tests = do
  print "eq8-p"
  let tape_init = [3,9,8,9,10,9,4,9,99,-1,8]
  print $ test tape_init 8 1
  print $ test tape_init 7 0
  print "lt8-p"
  let tape_init = [3,9,7,9,10,9,4,9,99,-1,8]
  print $ test tape_init 9 0
  print $ test tape_init 8 0
  print $ test tape_init 7 1
  print "eq8-i"
  let tape_init = [3,3,1108,-1,8,3,4,3,99]
  print $ test tape_init 8 1
  print $ test tape_init 7 0
  print "lt8-i"
  let tape_init = [3,3,1107,-1,8,3,4,3,99]
  print $ test tape_init 9 0
  print $ test tape_init 8 0
  print $ test tape_init 7 1
  print "jmp-p"
  let tape_init = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]
  print $ test tape_init 0 0
  print $ test tape_init 1 1
  print "jmp-i"
  let tape_init = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]
  print $ test tape_init 0 0
  print $ test tape_init 1 1
  print "ex"
  let tape_init =  [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]
  print $ test tape_init 7 999
  print $ test tape_init 8 1000
  print $ test tape_init 9 1001


main :: IO()
main = do
  input <- readFile "day05input"
  let tape_init = read_comma_separated_ints input
  let p1_vm = VM5.VM {
    VM5.program_counter = 0
    , VM5.tape = tape_init
    , VM5.inputs = [1]
    , VM5.outputs = []
    }
  let p1 = last (VM5.outputs $ VM5.runVM p1_vm)
  putStrLn ("part 1: " ++ (show p1))
  let p2_vm = VM5.VM {
    VM5.program_counter = 0
    , VM5.tape = tape_init
    , VM5.inputs = [5]
    , VM5.outputs = []
    }
  let p2 = last (VM5.outputs $ VM5.runVM p2_vm)
  putStrLn ("part 2: " ++ (show p2))