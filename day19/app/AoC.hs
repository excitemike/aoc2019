module Main (main) where

import Data.Bits
import Data.Char
import Graphics.Gloss
import qualified Data.Map.Strict as Map
import Debug.Trace
import Numeric
import System.IO
import qualified VM9

type XY = (Int,Int)


window = InWindow "AoC" (800, 800) (10, 10)

colorAt :: (Int,Int)->Color
colorAt (x,y)
    | i==0 = rose
    | i==1 = violet
    | i==2 = azure
    | i==3 = aquamarine
    | i==4 = chartreuse
    | i==5 = orange
  where i = mod (x*2+y+(quot x 3)) 6

          
argmax :: Ord b => (a->b)->[a]->a
argmax _ [] = error ":("
argmax f (x:xs') = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s


render :: [(XY,Int)]->Picture
render state = pictures $ subPictures
  where place (x,y) =
            translate 
              (10 * (fromIntegral x) - 250)
              (10 * (fromIntegral y) - 250)
        draw (xy, 0) = place xy $ color (colorAt xy) $ rectangleSolid 2 2
        draw (xy, 1) = place xy $ color (colorAt xy) $ rectangleSolid 10 10
        tilePictures = map draw state
        label = place (0,-10) 
                $ scale 0.25 0.25
                $ color white
                $ text
                $ ("p1: "++)
                $ show
                $ length
                $ filter ((==1) . snd)
                $ seq state state
        subPictures = label:tilePictures


main :: IO()
main = do
    input <- readFile "input"
    let vm = makeVm input
    {-let pointData = isAffected vm
      seq pointData
      $ display window black
      $ render pointData
      -}
    p2Point@(p2x,p2y) <- p2Search (1500,2000) (2000,2500) vm
    putStrLn 
        $ (("p2: "++(show p2Point)++" => ")++)
        $ show
        $ p2Formula
        $ p2Point 
    let beam = color (light blue)
               $ polygon $ polyPath vm (p2x, p2x)
    let santaBox = translate (fromIntegral (p2x+50)) (fromIntegral (p2y+50))
                   $ color black
                   $ rectangleWire 100 100
    let santaBox2 = translate (fromIntegral (p2x)) (fromIntegral (p2y))
                    $ pictures [ translate   0   0 $ color red    $ circleSolid 2 
                               , translate 100   0 $ color green  $ circleSolid 2
                               , translate 100 100 $ color blue   $ circleSolid 2
                               , translate   0 100 $ color yellow $ circleSolid 2
                               ]
    display window white 
        $ scale 1.0 1.0
        $ translate (fromIntegral (0-p2x-50)) (fromIntegral (0-p2y-50))
        $ pictures [beam, santaBox, santaBox2]
  where p2Formula (x,y) = x * 10000 + y
        bottomEdgePoint :: VM9.VM->XY->Point
        bottomEdgePoint vm xy@(x,y) = (fromIntegral x, fromIntegral $ minY xy vm) 
        topEdgePoint :: VM9.VM->Point->Point
        topEdgePoint vm xy@(x,_) = (x, topY vm xy) 
        bottomPath :: VM9.VM->XY->Path
        bottomPath vm xy@(x,y) = [ bottomEdgePoint vm (x', y) | x'<-[(x-10),(x+20)..(x+110)] ]
        topPath :: VM9.VM->Path->Path
        topPath vm other = [ topEdgePoint vm xy | xy<-(reverse other) ]
        polyPath :: VM9.VM->XY->[Point]
        polyPath vm xy = let ps = (bottomPath vm xy) 
                         in (ps ++ (topPath vm ps))


minY :: XY->VM9.VM->Int
minY xy@(x,y) vm = case doPointTest vm xy of
                      0 -> minY (x,y+1) vm
                      1 -> y


topY :: VM9.VM->Point->Float
topY vm bottom@(x,y) = topYBinSearch y (y+1000)
  where topYBinSearch :: Float->Float->Float
        topYBinSearch minY maxY
          | minY>=(maxY-1) = minY
          | otherwise      = let midY = (minY+maxY) / 2
                               in case doPointTest vm (floor x, floor midY) of 
                                    0 -> topYBinSearch minY midY
                                    1 -> topYBinSearch midY maxY
  
  
p2Search :: XY->XY->VM9.VM->IO XY
p2Search minXY@(minX,minY) maxXY@(maxX,maxY) vm
    | (minX>=maxX) && (minY>=(maxY-1)) 
    = do
        -- done
        putStrLn $ "search has ended " ++ (show [lowerLeftGood, lowerRightGood, upperLeftGood, upperRightGood])
        putStrLn $ "sanitycheck " ++ (show $ map (doPointTest vm) [(minX-1, minY),(minX+98, minY),(minX+98, minY+99),(minX-1, minY+99)])
        putStrLn $ "sanitycheck " ++ (show $ map (doPointTest vm) [(minX, minY-1),(minX+99, minY-1),(minX+99, minY+98),(minX, minY+98)])
        return (minX, minY)
    | (minY>=(maxY-1))
    = do
        -- y search ended, but maybe not successfully
        putStrLn $ "y search ended @ " ++ (show minY)
        putStrLn $ "  ulgood " ++ (show $ upperLeftGood)
        putStrLn $ "  sanitycheck " ++ (show $ lowerRightGood)
        
        if upperLeftGood
        then do
               putStrLn $ "  x="++(show midX)++" is small enough"
               p2Search (minX, minX) (midX, 2 * minX) vm
        else do
               putStrLn $ "  x="++(show midX)++" is too small"
               p2Search (midX+1, midX) (maxX, 2 * maxX) vm
    | minYGood
    = do
        -- search Y
        putStrLn $ "searching from min y of " ++ (show minY) ++ " " ++ (show (minXY, maxXY))
        
        if upperLeftGood
        then do
                putStrLn $ "  y="++(show midY)++" is low enough"
                p2Search (minX, minY) (maxX, midY) vm 
        else do
                putStrLn $ "  y="++(show midY)++" is too high"
                p2Search (minX, minY) (maxX, midY-1) vm
    | otherwise
    = do
        -- make sure we get a good starting point for searching Y
        putStrLn $ "minY needs fixing " ++ (show (minXY, maxXY))
        
        p2Search (minX, fixMinY minY) (maxX, maxY) vm
  where midX = quot (minX + maxX) 2
        midY = quot (minY + maxY) 2
        lowerLeftGood       = 1 == (doPointTest vm (midX, midY))
        lowerRightGood      = 1 == (doPointTest vm (midX+99, midY))
        upperLeftGood       = 1 == (doPointTest vm (midX, midY+99))
        upperRightGood      = 1 == (doPointTest vm (midX+99, midY+99))
        minYGood = 1 == (doPointTest vm (midX+99, minY))
        fixMinY y       = case doPointTest vm (midX+99, y) of
                            0 -> fixMinY (y+1)
                            1 -> y


makeVm :: String->VM9.VM
makeVm = (\tapeInit -> VM9.makeVM tapeInit []) . readTape
  
  
testPoints :: [XY]
testPoints = [ (x,y) | x <- [0..49], y <- [0..49] ]
        
        
isAffected :: VM9.VM->[(XY,Int)]
isAffected vm = map (\xy->(xy, doPointTest vm xy)) testPoints
        
        
doPointTest :: VM9.VM -> XY -> Int
doPointTest vm (x,y) = snd
                       $ VM9.runToOutput
                       $ VM9.sendInputs vm [x,y]
      
      
      
      
      
      
      
      
      
      
      
      
      
      