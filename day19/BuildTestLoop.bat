@echo off
setlocal

:start
cls
set WAITCOUNT=0
set MAXWAIT=60

start /B stack run

:wait
if %WAITCOUNT% GEQ %MAXWAIT% goto kill
timeout /t 1 > NUL
set /A WAITCOUNT+=1
FOR /F "delims=" %%a IN ('TASKLIST ^| FIND /C "stack.exe"') DO IF %%a EQU 0 GOTO nokill
goto wait

:kill
taskkill /im AoC-exe.exe /f

:nokill
pause
goto start

:error
echo ERROR
pause
goto start