import Data.Array
import Control.Concurrent
import Control.Monad
import Data.Char
import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import System.IO


type Deck = [Integer]
type LCG = (Integer,Integer)


factoryOrder :: Deck
factoryOrder = [0..10006]


dealWithIncrement :: Integer->Deck->Deck
dealWithIncrement n deck =
    map snd 
    $ Map.toAscList
    $ dealWithIncrement_ n 0 deck $ Map.empty
  where dealWithIncrement_ :: Integer->Integer->[Integer]->(Map.Map Integer Integer)->(Map.Map Integer Integer)
        dealWithIncrement_ increment current [] deckMap = deckMap
        dealWithIncrement_ increment current (top:rest) deckMap = 
            let { deckMap' = Map.insert current top deckMap
                ; current' = (mod (current + increment) (fromIntegral $ length deck))
                }
            in dealWithIncrement_ increment current' rest deckMap'


p1Process :: [String]->Deck->Deck
p1Process [] deck = deck
p1Process (line:rest) deck = p1Process rest deck'
  where deck'   | isPrefixOf "deal into new stack" line = reverse deck
                | isPrefixOf "cut -" line = let { n = read $ drop 5 line
                                                ; (top, bottom) = splitAt ((length deck)-n) deck
                                                }
                                            in bottom ++ top
                | isPrefixOf "cut " line =  let { n = read $ drop 4 line
                                                ; (top, bottom) = splitAt n deck
                                                }
                                            in bottom ++ top
                | isPrefixOf "deal with increment " line = 
                    let n = read $ drop 20 line
                    in dealWithIncrement n deck


lineToLcg :: Integer->String->LCG
lineToLcg numCards line 
    | isPrefixOf "deal into new stack" line
    = ((numCards-1),(numCards-1))
    | isPrefixOf "cut -" line 
    = let n = (read $ drop 5 line)
      in (1,n)
    | isPrefixOf "cut " line 
    = let n = (read $ drop 4 line)
      in (1,numCards-n)
    | isPrefixOf "deal with increment " line
    = let n = read $ drop 20 line
      in (n,0)


-- I still don't really understand this :(
inverseMultiplyMod :: Integer->Integer->Integer
inverseMultiplyMod a m = process 0 1 m a
  where
    process t newt r 0 = 
        if r > 1 then error "not invertible"
        else if t < 0 then (t + m) else t
    process t newt r newr =
        let { q = quot r newr
            ; t' = newt
            ; newt' = t - q * newt
            ; r' = newr
            ; newr' = r - q * newr
            }
        in process t' newt' r' newr'


invertLcg :: Integer->LCG->LCG
invertLcg numCards (a,b) = (a',b')
  where a' = inverseMultiplyMod a numCards
        b' = (-1) * a' * b


composeLcgs :: LCG->LCG->LCG
composeLcgs (a1,b1) (a2,b2) = (a1*a2, (a2*b1)+b2)


powMod :: Integer->Integer->Integer->Integer
powMod base 1 m = mod base m
powMod base pow m | even pow = (\x->mod x m) 
                               $ (powMod base (div pow 2) m) ^ 2
                  | odd pow  = (\x->mod x m)
                               $ ((powMod base (div (pow-1) 2) m) ^ 2) * base


repeatedLcg :: Integer->Integer->LCG->LCG
repeatedLcg numCards numReps (a,b) = (a',b')
  where
    a' = powMod a numReps numCards
    tmp = powMod a numReps ((a-1) * numCards)
    b' = b * (quot (tmp-1) (a-1))


p1 :: String->Maybe Int
p1 input = elemIndex 2019
           $ p1Process (lines input) factoryOrder


main :: IO()
main = do
    input <- readFile "input"
    {-
    putStrLn
        $ ("ex1 (should be 0 3 6 9 2 5 8 1 4 7):\n"++)
        $ show
        $ p1Process ["deal with increment 7","deal into new stack","deal into new stack"] [0..9]
    putStrLn
        $ ("ex2 (should be 3 0 7 4 1 8 5 2 9 6):\n"++)
        $ show
        $ p1Process ["cut 6","deal with increment 7","deal into new stack"] [0..9]
    putStrLn
        $ ("ex3 (should be 6 3 0 7 4 1 8 5 2 9):\n"++)
        $ show
        $ p1Process ["deal with increment 7","deal with increment 9","cut -2"] [0..9]
    putStrLn
        $ ("ex4 (should be 9 2 5 8 1 4 7 0 3 6):\n"++)
        $ show
        $ p1Process ["deal into new stack","cut -2","deal with increment 7","cut 8","cut -4","deal with increment 7","cut 3","deal with increment 9","deal with increment 3","cut -1"] [0..9]
    -}
    --putStrLn $ "p1: " ++ (show $ p1 input)
    putStrLn 
        $ ("p1 check (should be 2019): "++)
        $ show
        $ (\(a,b)-> mod (p1Slot*a + b) p1NumCards)
        $ repeatedLcg p1NumCards p1Iters
        $ invertLcg p1NumCards
        $ foldl composeLcgs (1,0)
        $ map (lineToLcg p1NumCards)
        $ lines
        $ input
    putStrLn 
        $ ("p2: "++)
        $ show
        $ (\(a,b)-> mod (p2Slot*a + b) p2NumCards)
        $ repeatedLcg p2NumCards p2Iters
        $ invertLcg p2NumCards
        $ foldl composeLcgs (1,0)
        $ map (lineToLcg p2NumCards)
        $ lines
        $ input
  where 
    p1NumCards = 10007
    p1Iters = 1
    p1Slot = 2558
    p2NumCards = 119315717514047
    p2Iters = 101741582076661
    p2Slot = 2020
                                           
    














