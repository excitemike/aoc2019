import System.IO 
import Control.Monad

digits :: Int->[Int]->[Int]
digits x digit_list =
  if x <= 0
  then digit_list
  else digits (quot x 10) ((mod x 10) : digit_list)

none_decreasing :: [Int]->Bool
none_decreasing xs = 
  and $ zipWith (<=) xs (tail xs)

some_adjacent_digits_the_same :: [Int]->Bool
some_adjacent_digits_the_same xs = 
  or $ zipWith (==) xs (tail xs)

test_password_p1 :: Int->Bool
test_password_p1 x =
  (100000 <= x) && (x <= 999999) -- six digits
  && let digits_list = digits x []
    in 
    (none_decreasing digits_list) -- no digits decreasing
    && (some_adjacent_digits_the_same digits_list) -- some adjacent doubles

p2_test_int :: [Int]->Bool
p2_test_int digit_list = 
  if (null digit_list)
  then False
  else
    let {
      initial_seq = takeWhile (\x -> x == (digit_list!!0)) digit_list;
      n = length initial_seq }
    in
      if n == 2
      then True
      else p2_test_int (drop n digit_list)

p2_test :: Int->Bool
p2_test x = p2_test_int $ digits x []

main :: IO()
main = do
  let passwords_p1 = filter test_password_p1 [367479..893698]
  putStrLn ("part 1: " ++ (show $ length passwords_p1))
  let passwords_p2 = filter p2_test passwords_p1
  putStrLn ("part 2: " ++ (show $ length passwords_p2))
  