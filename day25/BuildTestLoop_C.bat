@echo off
setlocal
set SRCDIR=%~dp0
set BASENAME=main
set SRC=%SRCDIR%%BASENAME%.c
set EXE=%SRCDIR%%BASENAME%_c.exe
set GCC=gcc

:start
cls
if not exist %EXE% goto build
echo Deleting %EXE%...
del %EXE%

:build
echo Building %EXE%...
%GCC% %SRC% -O -o %EXE%
if not exist %EXE% goto error

:run
echo Running %EXE%...
%EXE%

pause
goto start

:error
echo ERROR
pause
goto start