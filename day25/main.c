#include <string.h>
#include <stdio.h>
#include "vm.c"

void initTape(const char* path, int64_t * tape, int tapeSize) {
    FILE * fp = fopen(path, "r");
    char c = '\0';
    int i = 0;
    for (int i=0; i<tapeSize; ++i) {
        int64_t value = 0;
        fscanf(fp, "%lld", &value);
        tape[i] = value;
        fscanf(fp, ",");
        if (feof(fp)) { break; }
    }
    fclose(fp);
}

void consumeAndPrintOutput(Vm* vm) {
    int64_t* p = vm->outputs;
    while (0 != *p) {
        putchar(*p);
        ++p;
    }
    putchar('\n');
    memset(vm->outputs, 0, sizeof(int64_t) * OUTPUT_BUFFER_SIZE);
}

void sendCommand(Vm* vm, char* command) {
    int i=0;
    for (; (0!=command[i])&&(i<INPUT_BUFFER_SIZE-1) ; ++i) {
        vm->inputs[i] = command[i];
    }
    if (i>=INPUT_BUFFER_SIZE-1) {
        FAIL("too much input");
    }
    vm->inputs[i++] = '\n';
}

void play(Vm* vm, char** script) {
    char **curCommand = script;
    while (!vm->halted && (NULL!=*curCommand)) {
        runUntilBlocked(vm);
        consumeAndPrintOutput(vm);
        sendCommand(vm, *curCommand);
        ++curCommand;
    }
    consumeAndPrintOutput(vm);
    runUntilBlocked(vm);
    consumeAndPrintOutput(vm);
}

int main() {
    int64_t tape[TAPE_SIZE];
    memset(tape, 0, TAPE_SIZE * sizeof(int64_t));
    initTape("intcode", tape, TAPE_SIZE);
    
    Vm vm = {
        .programCounter = 0,
        .tape = tape,
        .tapeSize = TAPE_SIZE,
        .relativeBase = 0,
        .inputs = {0},
        .outputs = {0},
        .halted = 0
    };

    char * script[] = {
        "west"            // science lab
        ,"south"           // passages
        ,"take polygon"
        ,"north"           // science lab
        ,"east"            // hull breach
        ,"north"           // Navigation
        ,"west"            // Arcade
        ,"take boulder"
        ,"east"            // Navigation
        ,"south"           // Hull Breach
        ,"south"           // crew quarters
        ,"south"           // storage
        ,"take tambourine"  
        ,"north"           // crew quarters
        ,"north"           // Hull Breach
        ,"north"           // Navigation
        ,"north"           // Observatory
        ,"take manifold"     
        ,"north"           // Hall
        ,"take hologram"   
        ,"south"           // Observatory
        ,"west"            // Holodeck
        ,"take fuel cell"  
        ,"south"           // Kitchen
        ,"east"            // Hot Chocoloate Fountain
        ,"south"           // Corridor
        ,"take fixed point"  
        ,"north"           // hot choccy
        ,"west"            // kitch
        ,"north"           // holodeck
        ,"north"           // stables
        ,"take wreath"
        ,"east"            // eng
        ,"east"            // sec
        ,"drop tambourine"
        ,"drop hologram"
        ,"drop fuel cell"
        ,"drop wreath"
        ,"north"
        ,NULL
    }; 
    play(&vm, script);
    
    printf("\ndone\n");
    getc(stdin);
    return 0;
}