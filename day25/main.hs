import Control.Concurrent
import Control.Monad
import Data.Bits
import Data.Char
import Debug.Trace
import qualified Data.Map.Strict as Map
import Numeric
import System.IO
import qualified VM9


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s


makeVm :: String->VM9.VM
makeVm = (\tapeInit -> VM9.makeVM tapeInit []) . readTape


main :: IO()
main = 
    do
        input <- readFile "intcode"
        let vm = makeVm input
        let (vm', output) = VM9.runUntilBlocked vm
        putStrLn $ map chr output
        vm'' <- autoPlay autoPlayCommands vm'
        play vm''
  where
    autoPlayCommands :: [String]
    autoPlayCommands = ["west"            -- science lab
                       ,"south"           -- passages
                       ,"take polygon"
                       ,"north"           -- science lab
                       ,"east"            -- hull breach
                       ,"north"           -- Navigation
                       ,"west"            -- Arcade
                       ,"take boulder"
                       ,"east"            -- Navigation
                       ,"south"           -- Hull Breach
                       ,"south"           -- crew quarters
                       ,"south"           -- storage
                       ,"take tambourine"  
                       ,"north"           -- crew quarters
                       ,"north"           -- Hull Breach
                       ,"north"           -- Navigation
                       ,"north"           -- Observatory
                       ,"take manifold"     
                       ,"north"           -- Hall
                       ,"take hologram"   
                       ,"south"           -- Observatory
                       ,"west"            -- Holodeck
                       ,"take fuel cell"  
                       ,"south"           -- Kitchen
                       ,"east"            -- Hot Chocoloate Fountain
                       ,"south"           -- Corridor
                       ,"take fixed point"  
                       ,"north"           -- hot choccy
                       ,"west"            -- kitch
                       ,"north"           -- holodeck
                       ,"north"           -- stables
                       ,"take wreath"
                       ,"east"            -- eng
                       ,"east"            -- sec
                       ,"drop tambourine"
                       ,"drop hologram"
                       ,"drop fuel cell"
                       ,"drop wreath"
                       ,"north"
                       ,""
                       ]
    autoPlay :: [String]->VM9.VM->IO VM9.VM
    autoPlay [] vm = return vm
    autoPlay (command:rest) vm =
        do
            putStrLn command
            let (vm', output) = VM9.runUntilBlocked $ VM9.sendInputs vm $ map ord (command ++ "\n")
            putStrLn $ map chr output
            if (not $ VM9.halted vm')
            then autoPlay rest vm'
            else return vm
    play :: VM9.VM->IO ()
    play vm = do
                let (vm', output) = VM9.runUntilBlocked vm
                putStrLn $ map chr output
                command <- getLine
                when (not $ VM9.halted vm') (play $ VM9.sendInputs vm' $ map ord (command ++ "\n"))
