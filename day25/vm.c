#include <stdlib.h>
#include <stdint.h>

#define ADD_OPCODE (1)
#define MULTIPLY_OPCODE (2)
#define READ_INPUT_OPCODE (3)
#define OUTPUT_OPCODE (4)
#define JMP_IF_TRUE_OPCODE (5)
#define JMP_IF_FALSE_OPCODE (6)
#define LT_OPCODE (7)
#define EQ_OPCODE (8)
#define RELATIVE_BASE_OFFSET_OPCODE (9)
#define HALT_OPCODE (99)

#define POSITION_MODE (0)
#define IMMEIDATE_MODE (1)
#define RELATIVE_MODE (2)

#define TAPE_SIZE (5013)
#define INPUT_BUFFER_SIZE (32)
#define OUTPUT_BUFFER_SIZE (512)

#define FAIL(...) { printf(__VA_ARGS__); printf("\n"); __builtin_trap(); }

typedef int64_t (*BinaryOpFunc)(int64_t lhs, int64_t rhs);

typedef struct Vm {
    int programCounter;
    int64_t* tape;
    int tapeSize;
    int relativeBase;
    int64_t inputs[INPUT_BUFFER_SIZE];
    int64_t outputs[OUTPUT_BUFFER_SIZE];
    int halted;
} Vm;

void freeVm(Vm* vm) {
    free(vm->tape);
    vm->tape = 0;
    vm->tapeSize = 0;
}

int64_t paramLookup(Vm* vm, int64_t addr, int lookupType) {
    switch (lookupType) {
        case 0: return vm->tape[addr];
        case 1: return addr;
        case 2: return vm->tape[addr + vm->relativeBase];
    }
}

void executeBinaryOp(Vm* vm, BinaryOpFunc operation, int modeA, int modeB, int modeC) {
    int pc = vm->programCounter;
    int64_t* tape = vm->tape;
    int64_t paramA = tape[pc + 1];
    int64_t paramB = tape[pc + 2];
    int64_t paramC = tape[pc + 3];
    int64_t lhs = paramLookup(vm, paramA, modeA);
    int64_t rhs = paramLookup(vm, paramB, modeB);
    int64_t dst = (modeC==RELATIVE_MODE) ?
              (paramC + vm->relativeBase) :
              paramC;
    vm->programCounter = pc + 4;
    int64_t value = operation(lhs, rhs);
    if ((dst < 0) || (dst >= TAPE_SIZE)) {
        FAIL("Writing off of tape! (%ld)", dst);
    }
    tape[dst] = value;
}

int64_t addOperation(int64_t lhs, int64_t rhs) {
    return lhs + rhs;
}

int64_t multOperation(int64_t lhs, int64_t rhs) {
    return lhs * rhs;
}

int64_t lessThanOperation(int64_t lhs, int64_t rhs) {
    return lhs < rhs;
}

int64_t eqOperation(int64_t lhs, int64_t rhs) {
    return lhs == rhs;
}

void readInput(Vm* vm, int modeA, int modeB, int modeC) {
    int64_t value = vm->inputs[0];
    if (0 == vm->inputs[0]) {
        FAIL("no input ready for input instruction");
        return;
    }
    
    int64_t dstParam = vm->tape[vm->programCounter + 1];
    int64_t dstAddr = (RELATIVE_MODE==modeA) ? 
                   (dstParam + vm->relativeBase) :
                   dstParam;
    memmove(vm->inputs, vm->inputs+1, sizeof(int64_t) * (INPUT_BUFFER_SIZE-1));
    vm->tape[dstAddr] = value;
    vm->programCounter += 2;
}

void writeOutput(Vm* vm, int modeA, int modeB, int modeC) {
    int64_t srcParam = vm->tape[vm->programCounter + 1];
    int64_t srcValue = paramLookup(vm, srcParam, modeA);
    
    int i = 0;
    while ((i < OUTPUT_BUFFER_SIZE-1) && (vm->outputs[i] != 0)) {
        ++i;
    }
    if (i>=OUTPUT_BUFFER_SIZE-1) {
        FAIL("output buffer full");
    }
    vm->outputs[i] = srcValue;
    vm->outputs[i+1] = 0;
    vm->programCounter += 2;
}

void executeJmpIfTrue(Vm* vm, int modeA, int modeB, int modeC) {
    int64_t param = vm->tape[vm->programCounter + 1];
    int64_t value = paramLookup(vm, param, modeA);
    int64_t addrParam = vm->tape[vm->programCounter + 2];
    int64_t addr = paramLookup(vm, addrParam, modeB);
    vm->programCounter = (0==value) ? 
                         (vm->programCounter + 3) :
                         addr;
}

void executeJmpIfFalse(Vm* vm, int modeA, int modeB, int modeC) {
    int64_t param = vm->tape[vm->programCounter + 1];
    int64_t value = paramLookup(vm, param, modeA);
    int64_t addrParam = vm->tape[vm->programCounter + 2];
    int64_t addr = paramLookup(vm, addrParam, modeB);
    vm->programCounter = (0==value) ? 
                         addr :
                         (vm->programCounter + 3);
}

void executeRelativeBaseOffset(Vm* vm, int modeA, int modeB, int modeC) {
    int64_t addr = vm->tape[vm->programCounter + 1];
    int64_t value = paramLookup(vm, addr, modeA);
    vm->programCounter = vm->programCounter + 2;
    vm->relativeBase += value;
}

void executeOpcode(Vm* vm, int opcode, int modeA, int modeB, int modeC) {
    switch (opcode) {
        case ADD_OPCODE: executeBinaryOp(vm, addOperation, modeA, modeB, modeC); break;
        case MULTIPLY_OPCODE: executeBinaryOp(vm, multOperation, modeA, modeB, modeC); break;
        case READ_INPUT_OPCODE: readInput(vm, modeA, modeB, modeC); break;
        case OUTPUT_OPCODE: writeOutput(vm, modeA, modeB, modeC); break;
        case JMP_IF_TRUE_OPCODE: executeJmpIfTrue(vm, modeA, modeB, modeC); break;
        case JMP_IF_FALSE_OPCODE: executeJmpIfFalse(vm, modeA, modeB, modeC); break;
        case LT_OPCODE: executeBinaryOp(vm, lessThanOperation, modeA, modeB, modeC); break;
        case EQ_OPCODE: executeBinaryOp(vm, eqOperation, modeA, modeB, modeC); break;
        case RELATIVE_BASE_OFFSET_OPCODE: executeRelativeBaseOffset(vm, modeA, modeB, modeC); break;
        default: FAIL("unrecognized opcode");
    }
}

void stepVm(Vm* vm) {
    int64_t instruction = vm->tape[vm->programCounter];
    int opcode = (int)instruction % 100;
    if (opcode == HALT_OPCODE) {
        vm->halted = 1;
    } else {
        int mode1 = ((int)instruction / 100) % 10;
        int mode2 = ((int)instruction / 1000) % 10;
        int mode3 = ((int)instruction / 10000) % 10;
        executeOpcode(vm, opcode, mode1, mode2, mode3);
    }
}

void runUntilBlocked(Vm* vm) {
    int64_t instruction = vm->tape[vm->programCounter];
    int opcode = (int)instruction % 100;
    while (!vm->halted && !((READ_INPUT_OPCODE == opcode) && (0 == vm->inputs[0]))) {
        stepVm(vm);
        instruction = vm->tape[vm->programCounter];
        opcode = instruction % 100;
    }
}
