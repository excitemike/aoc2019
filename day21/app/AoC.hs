import Control.Concurrent
import Data.Bits
import Data.Char
import Debug.Trace
import qualified Data.Map.Strict as Map
import Numeric
import System.IO
import qualified VM9

type XY = (Int,Int)


argmax :: Ord b => (a->b)->[a]->a
argmax _ [] = error ":("
argmax f (x:xs') = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s


makeVm :: String->VM9.VM
makeVm = (\tapeInit -> VM9.makeVM tapeInit []) . readTape


p1 :: IO()
p1 = do
    putStrLn ""
    input <- readFile "intcode"
    springscript <- readFile "springscript"
    let vm0 = makeVm input
    let (vm1, output1) = VM9.runUntilBlocked vm0
    putStrLn $ map chr output1
    let program = map ord springscript
    putStr springscript
    let vm2 = VM9.sendInputs vm1 program
    let (vm3, output3) = VM9.runUntilBlocked vm2
    let (output4, output5) = break (>127) output3
    putStrLn $ map chr output4
    case output5 of
        (dmg:rest) | dmg >= 128 -> putStrLn ("p1: " ++ (show dmg))
    putStrLn ""

p2 :: IO()
p2 = do
    putStrLn ""
    input <- readFile "intcode"
    springscript <- readFile "springscriptrun.txt"
    let vm0 = makeVm input
    let (vm1, output1) = VM9.runUntilBlocked vm0
    putStrLn $ map chr output1
    let program = map ord springscript
    putStr springscript
    let vm2 = VM9.sendInputs vm1 program
    let (vm3, output3) = VM9.runUntilBlocked vm2
    let (output4, output5) = break (>127) output3
    putStrLn $ map chr output4
    case output5 of
        (dmg:output5) | dmg >= 128 -> putStrLn ("p2: " ++ (show dmg))
    putStrLn ""

main :: IO()
main = do
    p1
    p2
