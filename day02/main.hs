import Data.Char
import System.IO 
import Control.Monad
import Text.ParserCombinators.ReadP

eat_comma :: String->String
eat_comma s = 
  if (null s)
  then s
  else let c = s !! 0
    in if (c==',')
    then (drop 1 s)
    else s

read_comma_separated_ints :: String->[Int]
read_comma_separated_ints s = 
  if (null s)
  then []
  else let (i, s2) = (read_int s)
    in (i : (read_comma_separated_ints $ eat_comma s2))

read_digits :: String->(String,String)
read_digits s = 
  if (null s)
  then ("","")
  else let c = s !! 0
    in
      if (isDigit c)
      then let (c2, s2) = (read_digits (drop 1 s))
        in (c : c2, s2)
      else ("", s)

read_int :: String->(Int,String)
read_int s = let (s1, s2) = (read_digits s)
  in
    ((read::String->Int) s1, s2)

list_replace :: [Int]->Int->Int->[Int]
list_replace tape addr val =
  let (before,_:after) = (splitAt addr tape)
  in (before ++ (val : after))

execute_opcode :: Int->Int->[Int]->(Int, [Int])

execute_opcode 1 pc tape = 
  let {addr_a = (tape !! (pc+1));
    a = (tape !! addr_a);
    addr_b = (tape !! (pc+2));
    b = (tape !! addr_b);
    addr_c = (tape !! (pc+3));
    val = (a + b)}
    in interpret (pc+4) $ list_replace tape addr_c val
    
execute_opcode 2 pc tape = 
  let {addr_a = (tape !! (pc+1));
    a = (tape !! addr_a);
    addr_b = (tape !! (pc+2));
    b = (tape !! addr_b);
    addr_c = (tape !! (pc+3));
    val = (a * b)}
    in interpret (pc+4) $ list_replace tape addr_c val
    
execute_opcode 99 pc tape = 
  (pc, tape)

interpret :: Int->[Int]->(Int, [Int])
interpret pc tape = execute_opcode (tape !! pc) pc tape

day_2 :: [Int]->Int->Int->IO(Int)
day_2 input_tape noun verb = do
  let { tape2 = list_replace input_tape 1 noun;
        tape3 = list_replace tape2 2 verb;
        (pc, tape4) = interpret 0 tape3 }
    in return $ tape4 !! 0

day_2_p2_step :: [Int]->Int->Int->IO(Int, Int)
day_2_p2_step input_tape noun verb = do
  result <- day_2 input_tape noun verb
  if (result == 19690720)
  then return (noun, verb)
  else if (verb<100)
    then day_2_p2_step input_tape noun (verb+1)
    else if (noun<100)
      then day_2_p2_step input_tape (noun+1) 0
      else error "wtf"

day_2_p2 :: [Int]->IO(Int,Int)
day_2_p2 input = 
  day_2_p2_step input 0 0

main :: IO()
main = do
  input <- readFile "day02input"
  let input_tape = read_comma_separated_ints input
  p1 <- (day_2 input_tape 12 2)
  (noun, verb) <- (day_2_p2 input_tape)
  putStrLn ("part 1: " ++ (show p1))
  putStrLn ("part 2: " ++ (show (noun * 100 + verb)))