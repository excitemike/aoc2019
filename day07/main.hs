import Data.Char
import Data.Function
import Data.List
import System.IO

import qualified VM5

readTape :: String->[Int]
readTape s =
  let s2 = dropWhile (==',') s
  in
    if null s2
    then []
    else case span (/=',') s2 of
      ("","") -> []
      (a,"") -> (read a):[]
      ("",b) -> readTape b
      (a,_:b) -> (read a):(readTape b)
    


makeVM :: [Int]->[Int]->VM5.VM
makeVM tapeInit inputs = VM5.VM
    { VM5.program_counter = 0
    , VM5.tape = tapeInit
    , VM5.inputs = inputs
    , VM5.outputs = []
    }


runAmplifier :: [Int]->Int->Int->Int
runAmplifier tapeInit prevOutput phase =
  makeVM tapeInit [phase, prevOutput]
  & VM5.runVM
  & VM5.outputs
  & (!!0)


stepP2 :: VM5.VM->[Int]->(VM5.VM, Int)
stepP2 vm inputs = 
  let { newInputs = (VM5.inputs vm)++inputs
      ; (vm', output) = VM5.runToOutput $ vm { VM5.inputs = newInputs }
      }
  in (vm', output)


runOneLoopP2 :: [VM5.VM]->[Int]->([VM5.VM], Int)
runOneLoopP2 vms inputs =
  if null vms
  then let i:_=inputs in ([], i)
  else let 
    { (first:rest) = vms
    ; (first', outputOfFirst) = stepP2 first inputs
    ; (rest', output) = runOneLoopP2 rest (outputOfFirst:[])
    }
    in (first' : rest', output)


runP2 :: Int->[VM5.VM]->([VM5.VM], Int)
runP2 prevOutput vms =
  if any VM5.halted vms
  then (vms, prevOutput)
  else 
    let (vms', output) = runOneLoopP2 vms (prevOutput:[])
    in if any VM5.halted vms'
      then (vms', output)
      else runP2 output vms'


testPermutation :: [Int]->[Int]->Int
testPermutation tapeInit phases =
  foldl (runAmplifier tapeInit) 0 phases
  

testPermutationP2 :: [Int]->[Int]->Int
testPermutationP2 tapeInit phases
  = map ((makeVM tapeInit) . (:[])) phases
  & runP2 0
  & snd


main :: IO()
main = do
  input <- readFile "day07input"
  putStrLn
    $ ("part 1: " ++)
    $ show
    $ maximum
    $ map (testPermutation $ readTape input)
    $ permutations [0..4]
  putStrLn 
    $ ("part 2: " ++)
    $ show
    $ maximum
    $ map (testPermutationP2 $ readTape input)
    $ permutations [5..9]