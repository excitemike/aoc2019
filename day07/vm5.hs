module VM5(VM(..), runUntilBlocked, runToOutput, runVM, stepVM, halted) where

addOpcode = 1
multiplyOpcode = 2
readInputOpcode = 3
outputOpcode = 4
jmpIfTrueOpcode = 5
jmpIfFalseOpcode = 6
ltOpcode = 7
eqOpcode = 8
haltOpcode = 99

positionMode = 0
immediateMode = 1

data VM = VM
  { program_counter :: Int
  , tape :: [Int]
  , inputs :: [Int]
  , outputs :: [Int]
  }
  deriving(Show)


list_replace :: [Int]->Int->Int->[Int]
list_replace tape addr val =
  let (before,_:after) = (splitAt addr tape)
  in (before ++ (val : after))


param_lookup :: VM5.VM->Int->Int->Int
param_lookup vm param 0 = (VM5.tape vm) !! param
param_lookup vm param 1 = param


executeBinaryOp :: VM5.VM->(Int->Int->Int)->Int->Int->Int->VM5.VM
executeBinaryOp vm operation mode_a mode_b mode_c =
  let 
    { pc = VM5.program_counter vm
    ; tape = VM5.tape vm
    ; param_a = tape !! (pc + 1)
    ; param_b = tape !! (pc + 2)
    ; param_c = tape !! (pc + 3)
    ; a = param_lookup vm param_a mode_a
    ; b = param_lookup vm param_b mode_b
    }
    in vm { program_counter = pc + 4
          , tape = list_replace tape param_c (operation a b)
          }


executeOpcode :: VM->Int->Int->Int->Int->VM

-- add
executeOpcode vm 1 mode_a mode_b mode_c
  = executeBinaryOp vm (+) mode_a mode_b mode_c
  
-- multiply
executeOpcode vm 2 mode_a mode_b mode_c
  = executeBinaryOp vm (*) mode_a mode_b mode_c

-- input
executeOpcode vm 3 mode_a mode_b mode_c
  = if null $ inputs vm
  then error $ "no inputs ready for input instruction"
  else let
      { dst = (tape vm) !! ((program_counter vm) + 1)
      ; (value:_, inputs') = splitAt 1 $ inputs vm
      }
    in vm
      { program_counter = (program_counter vm) + 2 
      , inputs = inputs'
      , tape = list_replace (tape vm) dst value
      }

-- output
executeOpcode vm 4 mode_a mode_b mode_c
  = let { src_param = (tape vm) !! ((program_counter vm) + 1)
        ; src_value = param_lookup vm src_param mode_a
        }
    in vm
      { program_counter = (program_counter vm) + 2
      , outputs = (outputs vm) ++ (src_value : [])
      }

-- jump if true
executeOpcode vm 5 mode_a mode_b mode_c
  = let { param = (tape vm) !! ((program_counter vm) + 1)
        ; value = param_lookup vm param mode_a
        ; addr_param = (tape vm) !! ((program_counter vm) + 2)
        ; addr = param_lookup vm addr_param mode_b
        }
    in vm { program_counter = if value == 0 then (program_counter vm) + 3 else addr }
    
-- jump if false
executeOpcode vm 6 mode_a mode_b mode_c
  = let { param = (tape vm) !! ((program_counter vm) + 1)
        ; value = param_lookup vm param mode_a
        ; addr_param = (tape vm) !! ((program_counter vm) + 2)
        ; addr = param_lookup vm addr_param mode_b
        }
    in vm { program_counter = if value == 0 then addr else (program_counter vm) + 3 }

-- less than
executeOpcode vm 7 mode_a mode_b mode_c
  = executeBinaryOp vm (\a b -> if a < b then 1 else 0) mode_a mode_b mode_c

-- equals
executeOpcode vm 8 mode_a mode_b mode_c
  = executeBinaryOp vm (\a b -> if a == b then 1 else 0) mode_a mode_b mode_c


halted :: VM->Bool
halted vm = (==99) (mod ((tape vm) !! (program_counter vm)) 100)


runUntilBlocked :: VM->VM
runUntilBlocked vm
  = let 
    { instruction = (tape vm) !! (program_counter vm)
    ; opcode = mod instruction 100
    }
  in if opcode == haltOpcode 
    then vm
    else if (opcode == readInputOpcode) && (null $ inputs vm)
      then vm
      else 
        runUntilBlocked $ stepVM vm


runToOutput :: VM->(VM, Int)
runToOutput vm
  = if not $ null $ outputs vm
  then
    let { o:outputs' = outputs vm
        ; vm' = vm { outputs = outputs' }
        }
    in (vm', o)
  else
    let 
      { instruction = (tape vm) !! (program_counter vm)
      ; opcode = mod instruction 100
      ; vm2 = stepVM vm
      ; o2:outputs2 = outputs vm2
      ; vm3 = vm2 { outputs = outputs2 }
      }
    in if opcode == haltOpcode 
      then error "program halted before producing output"
      else if (opcode == readInputOpcode) && (null $ inputs vm)
        then error "program blocked on input before producing output"
        else if opcode == outputOpcode
          then (vm3, o2)
          else runToOutput vm2


runVM :: VM->VM
runVM vm = 
  let 
    { instruction = (tape vm) !! (program_counter vm)
    ; opcode = mod instruction 100
    }
  in if opcode == haltOpcode 
    then vm
    else runVM $ stepVM vm


stepVM :: VM->VM
stepVM vm = 
  let 
    { instruction = (tape vm) !! (program_counter vm)
    ; opcode = mod instruction 100
    ; mode1 = mod (quot instruction 100) 10
    ; mode2 = mod (quot instruction 1000) 10
    ; mode3 = mod (quot instruction 10000) 10 
    }
  in if opcode == haltOpcode 
    then vm
    else executeOpcode vm opcode mode1 mode2 mode3