import System.IO 
import Control.Monad

fuel_req :: Int -> Int
fuel_req module_mass = 
  if module_mass > 5
  then (quot module_mass 3) - 2
  else 0

fuel_req2 :: Int -> Int
fuel_req2 module_mass = 
  let additional_fuel = fuel_req module_mass
  in 
    if additional_fuel > 5
    then additional_fuel + (fuel_req2 additional_fuel)
    else additional_fuel

day_1 :: String->(Int->Int)->IO(Int)
day_1 filename fuel_rec_func = do
  file_contents <- (readFile filename)
  return $ foldl (+) 0 $ map (fuel_rec_func . read::String->Int) $ lines file_contents

main :: IO()
main = do
  p1 <- day_1 "day01input" fuel_req
  p2 <- day_1 "day01input" fuel_req2
  putStrLn  ("part 1: " ++ (show p1))
  putStrLn  ("part 2: " ++ (show p2))