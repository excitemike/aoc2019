module Main (main) where

import Control.Concurrent
import Data.Bits
import Data.Char
import Debug.Trace
import Graphics.Gloss
import qualified Data.Map.Strict as Map
import Numeric
import System.IO
import qualified VM9

type XY = (Int,Int)
type Network = Map.Map Int VM9.VM
type Packet = (Int,Int,Int)


window = InWindow "AoC" (800, 800) (10, 10)

colorAt :: (Int,Int)->Color
colorAt (x,y)
    | i==0 = rose
    | i==1 = violet
    | i==2 = azure
    | i==3 = aquamarine
    | i==4 = chartreuse
    | i==5 = orange
  where i = mod (x*2+y+(quot x 3)) 6


argmax :: Ord b => (a->b)->[a]->a
argmax _ [] = error ":("
argmax f (x:xs') = foldl (\x1 x2-> if (f x1)>(f x2) then x1 else x2) x xs'


readTape :: String->[Int]
readTape s | null s2 = []
           | otherwise = case span (/=',') s2 of
                           ("","") -> []
                           (a,"") -> (read a):[]
                           ("",b) -> readTape b
                           (a,_:b) -> (read a):(readTape b)
  where s2 = dropWhile (==',') s


makeVm :: String->VM9.VM
makeVm = (\tapeInit -> VM9.makeVM tapeInit []) . readTape


render :: [(XY,Int)]->Picture
render state = pictures $ subPictures
  where place (x,y) =
            translate 
              (10 * (fromIntegral x) - 250)
              (10 * (fromIntegral y) - 250)
        draw (xy, 0) = place xy $ color (colorAt xy) $ rectangleSolid 2 2
        draw (xy, 1) = place xy $ color (colorAt xy) $ rectangleSolid 10 10
        tilePictures = map draw state
        label = place (0,-10) 
                $ scale 0.25 0.25
                $ color white
                $ text
                $ ("p1: "++)
                $ show
                $ length
                $ filter ((==1) . snd)
                $ seq state state
        subPictures = label:tilePictures


makeNetwork :: VM9.VM->Network
makeNetwork vm = Map.fromList $ map (\i->(i, VM9.sendInputs vm [i])) [0..49]


-- returns the updated network and the first packet sent to 255, if any
stepNetwork :: Network->(Network, Maybe Packet)
stepNetwork network = stepNetwork_ 0 network


-- step the whole network, erturn the packet sent to 255 if any
stepNetwork_ :: Int->Network->(Network, Maybe Packet)
stepNetwork_ i network = case Map.lookup i network of
                            Nothing -> (network, Nothing)
                            Just vm -> stepVM i network vm


-- step one vm in the network, return the packet sent to 255 if any
stepVM :: Int->Network->VM9.VM->(Network, Maybe Packet)
stepVM i network vm = 
    case outputs' of
        []              ->  let { vm'' = VM9.sendInputs vm' [(-1)]
                                ; network'' = Map.insert i vm'' network'
                                }
                            in stepNetwork_ (i+1) network''
        (a:[])          ->  let { vm'' = VM9.ungetOutput vm' [a]
                                ; network'' = Map.insert i vm'' network'
                                }
                            in stepNetwork_ (i+1) network''
        (a:b:[])        ->  let { vm'' = VM9.ungetOutput vm' [a,b]
                                ; network'' = Map.insert i vm'' network'
                                }
                            in stepNetwork_ (i+1) network''
        (addr:x:y:rest) ->  case addr of
                                255 ->  (network', Just (addr,x,y))
                                _   ->  let { vm'' = VM9.ungetOutput vm' rest
                                            ; network'' = Map.insert i vm'' network'
                                            ; network''' = routePacket (addr,x,y) network''
                                            }
                                        in stepNetwork_ (i+1) network'''
  where (vm', outputs) = VM9.runUntilBlocked vm
        network' = Map.insert i vm' network
        outputs' = case outputs of
                    [] -> outputs
                    (a:[]) -> error " not enough outputs"
                    (a:b:[]) -> error " not enough outputs"
                    (a:b:c:rest) -> outputs


routePacket :: Packet->Network->Network
routePacket packet@(addr,x,y) network = trace ("packet "++(show (x,y))++" sent to "++(show addr)) network'
  where vm' = case Map.lookup addr network of
                Nothing -> error $ "can't route packet with address "++(show addr)
                Just vm -> VM9.sendInputs vm [x,y]
        network' = Map.insert addr vm' network


-- returns the first packet found that is sent to address 255
p1Process :: Network->(Network, Packet)
p1Process network = 
    case maybePacket of
        Just packet -> (network', packet)
        Nothing     -> p1Process network'
  where (network', maybePacket) = stepNetwork network


p1 :: String->IO()
p1 = ( putStrLn
     . ("p1: " ++) 
     . show
     . snd
     . p1Process
     . makeNetwork
     . makeVm
     )


vmIsIdle :: VM9.VM->Bool
vmIsIdle vm = [(-1)]==inputs
  where inputs = (VM9.inputs vm)


p2Process :: Packet->Packet->Network->Int
p2Process prevRcvPacket@(_,rcvX,rcvY) prevSentPacket@(_,sentX,sentY) network = 
    if isDone then sentY
    else case maybePacket of
            Just rcv -> p2Process rcv           prevSentPacket' network''
            Nothing  -> p2Process prevRcvPacket prevSentPacket' network''
  where allIdle = all (vmIsIdle . snd) $ Map.toList network
        isDone = allIdle && (prevSentPacket' == prevSentPacket)
        prevSentPacket' = if allIdle
                          then (0,rcvX,rcvY)
                          else prevSentPacket
        network' =  if allIdle
                    then routePacket prevSentPacket' network
                    else network
        (network'', maybePacket) = stepNetwork network'


p2 :: String->Int
p2 = ( (p2Process (-1,-1,-1)(-1,-1,-1))
     . makeNetwork
     . makeVm
     )


main :: IO()
main = do
    input <- readFile "intcode"
    p1 input
    putStrLn "p2: "
    (putStr . show) (p2 input)
