@echo off
setlocal
set BASENAME=main
set SRC=%BASENAME%.hs
set EXE=%BASENAME%.exe

:start
set WAITCOUNT=0
set MAXWAIT=20
cls
if not exist %EXE% goto build
echo Deleting %EXE%...
del %EXE%

:: thing for Mark
::if not exist output goto build
::echo Deleting output...
::del output


:build
echo Building %EXE%...
ghc %SRC% -O -o %EXE%
if not exist %EXE% goto error

echo Running %EXE%...
::start /B cmd /c %EXE% 2^> output
start /B %EXE%

:wait
if %WAITCOUNT% GEQ %MAXWAIT% goto kill
timeout /t 2 > NUL
set /A WAITCOUNT+=2
FOR /F "delims=" %%a IN ('TASKLIST ^| FIND /C "%EXE%"') DO IF %%a EQU 0 GOTO nokill
goto wait

:kill
taskkill /im %EXE% /f

:nokill
pause
goto start

pause
goto start

:error
echo ERROR
pause
goto start