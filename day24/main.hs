import Data.Array
import Control.Concurrent
import Control.Monad
import Data.Bits
import Data.Char
import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Debug.Trace
import System.IO

    
type XY = (Int,Int)
type XYZ = (Int,Int,Int)
type BugMap = Map.Map XY Int
type BugMap2 = Map.Map XYZ Int
type Seen = Set.Set BugMap


stepCell :: XY->BugMap->Int
stepCell xy@(x,y) bugMap = case Map.lookup xy bugMap of 
                                Just 0  ->  if (numAdjacent==2) || (numAdjacent==1)
                                            then 1
                                            else 0
                                Just 1  ->  if (numAdjacent==1)
                                            then 1
                                            else 0
  where
    up      = (x,y-1)
    down    = (x,y+1)
    left    = (x-1,y)
    right   = (x+1,y)
    toInt xy = case Map.lookup xy bugMap of
                    Just n -> n
                    _      -> 0
    numAdjacent = sum $ map toInt [up,down,left,right]


stepMap :: BugMap->BugMap
stepMap bugMap =
    Map.fromList [((x, y), stepCell (x,y) bugMap) | x<-[0..4], y<-[0..4]]


rating :: BugMap->Int
rating bugMap = sum 
                $ map (\((x,y),b)->(b `shift` (y*5+x)))
                $ Map.toList 
                $ bugMap


p1Process :: Seen->BugMap->Int
p1Process seen bugMap = 
    if Set.member bugMap seen
    then rating bugMap
    else p1Process (Set.insert bugMap seen) (stepMap bugMap)
    
    
debugStr :: BugMap2->String
debugStr bugMap = concat $ map renderDepth [minDepth..maxDepth]
  where minDepth = minimum $ map (\(x,y,z)->z) $ Map.keys bugMap
        maxDepth = maximum $ map (\(x,y,z)->z) $ Map.keys bugMap
        renderDepth z = concat  [ "Depth "
                                , show z
                                , ":\n"
                                , concat    [ ("\t" ++ [renderCell (x,y,z) | x<-[(-2)..2] ] ++ "\n")
                                            | y<-[(-2)..2]
                                            ]
                                ]
        renderCell xyz@(x,y,z) = if (x,y)==(0,0) then '?'
                             else case Map.lookup xyz bugMap of
                                    Just 0 -> '.'
                                    Just 1 -> '#'
                                    _      -> '?'


p2Process :: Int->Int->BugMap2->Int
p2Process maxGens generation bugMap
    | generation >= maxGens
    = sum $ map snd $ Map.toList bugMap
    | otherwise 
    = {-trace (generationLabel ++ (debugStr bugMap')) $-} p2Process maxGens generation' bugMap'
  where
    generationLabel = "\n\nGeneration #" ++ (show generation') ++ ":\n\n\n"
    generation' = (generation+1)
    bugMap' = (stepMap2 generation' bugMap)


stepMap2 :: Int->BugMap2->BugMap2
stepMap2 generation bugMap =
    Map.fromList [ ((x,y,z), stepCell2 (x,y,z) bugMap) 
                 | x<-[(-2)..2]
                 , y<-[(-2)..2]
                 , z<-[minLevel..maxLevel]
                 , (x,y)/=(0,0)
                 ]
  where
    minLevel = 0-maxLevel
    maxLevel = (quot (generation+1) 2)


stepCell2 :: XYZ->BugMap2->Int
stepCell2 xyz@(x,y,z) bugMap =
    case Map.lookup xyz bugMap of 
        Just 1  ->  if (numAdjacent==1)
                    then 1
                    else 0
        _       ->  if (numAdjacent==2) || (numAdjacent==1)
                    then 1
                    else 0
  where
    xy      = (x,y)
    up      = if y==(-2)
              then [(0,-1,z-1)]
              else if xy==(0,1) 
              then [ (x,2,z+1) 
                   | x<-[(-2)..2]
                   ]
              else [(x,y-1,z)]
    down    = if xy==(0,(-1))
              then [ (x,(-2),z+1) 
                   | x<-[(-2)..2]
                   ]
              else if y==2
              then [ (0,1,z-1) ]
              else [(x,y+1,z)]
    left    = if x==(-2)
              then [((-1),0,z-1)]
              else if xy==(1,0) 
              then [ (2,y,z+1) 
                   | y<-[(-2)..2]
                   ]
              else [(x-1,y,z)]
    right   = if xy==((-1),0)
              then [ ((-2),y,z+1) 
                   | y<-[(-2)..2]
                   ]
              else if x==2
              then [ (1,0,z-1) ]
              else [(x+1,y,z)]
    toInt xy = case Map.lookup xy bugMap of
                    Just n -> n
                    _      -> 0
    numAdjacent = sum $ map toInt $ concat [up,down,left,right]
    

main :: IO()
main = do
    input <- readFile "input"
    let mapInit = readMap input 0 0 Map.empty
    putStrLn
      $ ("p1: "++)
      $ show
      $ p1Process Set.empty
      $ mapInit
    {-let exampleMapStr = "....#\n#..#.\n#.?##\n..#..\n#...."
    let exampleMapInit = readMap exampleMapStr 0 0 Map.empty
    putStrLn
      $ ("Mark's example: "++)
      $ show
      $ p2Process 200 0
      $ Map.fromList
      $ map (\((x,y),b)->((x-2,y-2,0),b))
      $ Map.toList
      $ exampleMapInit
    -}
    putStrLn
      $ ("p2: "++)
      $ show
      $ p2Process 200 0
      $ Map.fromList
      $ map (\((x,y),b)->((x-2,y-2,0),b))
      $ Map.toList
      $ mapInit
  where readMap [] x y bugMap = bugMap
        readMap (c:rest) x y bugMap = 
            case c of
                '#'  -> readMap rest (x+1)     y $ Map.insert (x,y) 1 bugMap
                '.'  -> readMap rest (x+1)     y $ Map.insert (x,y) 0 bugMap
                '\n' -> readMap rest     0 (y+1) $ Map.insert (x,y) 0 bugMap
                _    -> readMap rest     x     y bugMap
                                        
                                           
    














